import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.poi.hssf.usermodel.HSSFFormulaEvaluator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;


public class Report {
    static final String left2right_text = "";//"\u200E";
    static final String right2left_text = "";//"\u200F";
    static final String SECTION_REPORT_INPUT_FILE = "section-report-motorola.xls";
    static final String PRIVATE_TRAVEL_INPUT_FILE = "private-travel-motorola.xls";
    static final String OUTPUT_FILE = "output__report-motorola.xls";
    static final String SECTION_REPORT_OUTPUT_SHEET = "output_section";
    static final String SECTION_VOLUME_REPORT_OUTPUT_SHEET = "output_section-volume";
    static final String PRIVATE_TRAVEL_OUTPUT_SHEET = "output_private-travel";
    static final String REPORT_TEST_COUNT_OUTPUT_SHEET = "output_test-count";
    static final String TEST_LIST_OUTPUT_SHEET = "output_test-list";
    static final String minTimeBetweenDrives = "00:15:00";
    static final String CSV = ".csv";
    static final String[] TESTS = {"בדוח הSection",
            "00. בדיקת נפח נסיעות (כמות רכבים מתנדבים מספר נסיעות וכמות מקטעים)",
            "01. ערכים נכונים בכל עמודה ושאין עמודה ריקה(צריך טבלת ערכים אפשריים לכל עמודה)",
            "02. כאשר יש מקטע קודם בודק שהוא קיים ושהערכים שלו בשורה הזו לא NULL",
            "03. אם הערכים לא NULL בודק שהערכי הזמן, תאריך אורך ורוחב מתאימים שורות",
            "04. האם קוד קטע דרך מתאים לתעריף קטע דרך ועלות לפי שעות",
            "05. שהאורך שווה לק\"מ בסיום פחות ק\"מ בהתחלה",
            "06. כאשר משך נסיעה ארוך מרבע שעה לבדוק שמרחק הנסיעה גדול מ 0 וכאשר אורך נסיעה גדול מקילומטר הזמן גדול מ 0",
            "07. שיש YES רק במקטעים הראשונים של כל נסיעה ואין NO במקטעים ראשונים",
            "08. שתשלום מחושב נכון אם עלות לא 0 תשלום = (עלות לק\"מ + רמת זיהום)*מרחק אחרת 0",
            "09. ממוצע מהירות הגיוני מרחק כולל לחלק לזמן כולל וחיסור קילומטראז' חלקי חיסור זמנים",
            "10. מעבר מקטעים(פוליגונים) שמתחלפים מקטעים כשהרכב עובר בין פוליגונים",
            "11. שאין זמני המעבר בתוך מקטעים ושכל המקטע בתוך אותו אזור זמן",
            "12. שקוד הזמן מתאים לזמנים",
            "13. זמן התחלה קטן מזמן סיום וקילומטראז’ התחלה קטן קילומטראז’ סיום",
            "14. אין שני מקטעים עם אותו הקוד",
            "",
            "בדוח הPrivate",
            "01. ערכים נכונים בכל עמודה ושאין עמודה ריקה(צריך טבלת ערכים אפשריים לכל עמודה)",
            "02. שסכום זמני הנסיעה מה-SECTION שווה לזמן נסיעה",
            "03. השוואה בין מחיר ליום בSECTION  לתשלום בפועל",
            "04. תשלום בפועל בין 0 ל 25 כולל",
            "05. שהמרחק והזמן מתאימים פחות או יותר – מהירות",
            "06. לבדוק שהנסיעה הקודמת קיימת(שהערכי הזמן, תאריך אורך ורוחב מתאימים שורות)",
            "07. אם קיימת להשוות ערכים מתאימים",
            "08. אם יש YES בנסיעת 4 ק\"מ ראשונים G לבדוק שמרחק נסיעה K קטן מ 4 ושהנסיעה הקודמת לא נמצאת באותו היום ופעם אחת ביום לרכב בתאריך",
            "09. לוודא שבסיכום יש נ.צ של התחלת הראשון בהתחלה ובסיום של סיום האחרון",
            "10. אם עלות F לא 0 לבדוק ששינוי תעריף E כפול מרחק K שווה לחיוב/זיכוי זיהום D ו שעלות ועוד זיהום שווה לתשלום בפועל",
            "11. כאשר משך נסיעה ארוך מרבע שעה לבדוק שמרחק הנסיעה גדול מ 0 ולהפך",
            "12. אין שתי נסיעות עם אותו קוד",
            "13. אם אורך הנסיעה קטן מ 4 והיא הנסיעה הראשונה בתאריך לראות שמסומן YES ב 4 ק\"מ ראשונים",
            "14. שהנסיעה הקודמת הסתיימה לפני יותר מ"+minTimeBetweenDrives,
            "15. שהנסיעה הקודמת הסתיימה לפני שהנסיעה הזאת התחילה",
            ""
    };
    static int[] sectionTestCount = new int[14];
    static int[] privateTestCount = new int[15];




    static final String KEY_SEPERATOR = "||--||";

    static final double maxErrorLength = 0.01;
    static final double maxErrorPayment = 3;
    static final double maxCoordinatesError = 0.01;
    static final double maxErrorTime = 1;
    static final double maxErrorTimeChange = timeToSeconds("15:15:15");
    static final double minSecondsBetweenDrives = timeToSeconds(minTimeBetweenDrives);
    static final double maxAvgSpeed = 120;//kph
    static final double minAvgSpeed = 0;//kph
    static final double maxPayment = 25;
    static final double firstKMs = 4;
    static final int costCodeFree = 3;
    static final String minTime = "00:15:00";
    static final double minLength = 1;
    static final String[] times = {"06:45:00","09:30:00","15:30:00","18:30:00","20:00:00"};
    static final int[] timesCostCode = 	{	  1	   ,	 2	  ,		1	 ,	   2	,costCodeFree};
    static final String[] timesStart = 	{"06:45:00","09:30:00","15:30:00","18:30:00","20:00:00"};
    static final String[] timesEnd = 	{"09:30:00","15:30:00","18:30:00","20:00:00","30:45:00"/*"06:45:00"*/};

//    static final String[] rushHours = {"06:45:00","09:30:00","15:30:00","18:30:00"};
//    static final String[] lowHours = {"09:30:00","15:30:00","18:30:00","20:00:00"};
//    static final String[] freeHours = {"20:00:00","30:45:00"/*"06:45:00"*/};

    static final List<String> nullStrings = new LinkedList<>();
    static final List<String> zeroStrings = new LinkedList<>();
    static final List<String> zeroTimeStrings = new LinkedList<>();

    static HashMap<String,Integer> rates = new HashMap<>();
    static HashMap<String,Double> costsRush = new HashMap<>();
    static HashMap<String,Double> costsLow = new HashMap<>();

    static HashMap<String,Set<String>> sectionsPerDay = new HashMap<>();
    static HashMap<String,Set<String>> drivesPerDay = new HashMap<>();
    static HashMap<String,Set<String>> peoplePerDay = new HashMap<>();
    static HashMap<String,Integer> errorsPerDay = new HashMap<>();


    static final int SECTION_R_OUTPUT_CAR_ID_COL = 0;
    static final int SECTION_R_OUTPUT_DRIVE_ID_COL = 1;
    static final int SECTION_R_OUTPUT_SECTION_ID_COL = 2;
    static final int SECTION_R_OUTPUT_TEST01_COL = 3;
    static final int SECTION_R_OUTPUT_TEST02_COL = 4;
    static final int SECTION_R_OUTPUT_TEST03_COL = 5;
    static final int SECTION_R_OUTPUT_TEST04_COL = 6;
    static final int SECTION_R_OUTPUT_TEST05_COL = 7;
    static final int SECTION_R_OUTPUT_TEST06_COL = 8;
    static final int SECTION_R_OUTPUT_TEST07_COL = 9;
    static final int SECTION_R_OUTPUT_TEST08_COL = 10;
    static final int SECTION_R_OUTPUT_TEST09_COL = 11;
    static final int SECTION_R_OUTPUT_TEST10_COL = 12;
    static final int SECTION_R_OUTPUT_TEST11_COL = 13;
    static final int SECTION_R_OUTPUT_TEST12_COL = 14;
    static final int SECTION_R_OUTPUT_TEST13_COL = 15;
    static final int SECTION_R_OUTPUT_TEST14_COL = 16;
    static final int SECTION_R_OUTPUT_ALL_TESTS_COL = 17;

    static List<String> sectionColNamesList;
    static final int SECTION_R_SECTION_PAYMENT_COL = 4;
    static final int SECTION_R_SECTION_END_LENGTH_COL = 5;
    static final int SECTION_R_SECTION_START_LENGTH_COL = 6;
    static final int SECTION_R_DRIVE_ID_COL = 7;
    static final int SECTION_R_SECTION_ID_COL = 8;
    static final int SECTION_R_FIRST_SECTION_COL = 9;
    static final int SECTION_R_SECTION_LENGTH_COL = 10;
    static final int SECTION_R_PREVIOUS_SECTION_END_LATITUDE_COL = 11;
    static final int SECTION_R_PREVIOUS_SECTION_END_LONGITUDE_COL = 12;
    static final int SECTION_R_SECTION_END_LATITUDE_COL = 13;
    static final int SECTION_R_SECTION_END_LONGITUDE_COL = 14;
    static final int SECTION_R_SECTION_START_LATITUDE_COL = 15;
    static final int SECTION_R_SECTION_START_LONGITUDE_COL = 16;
    static final int SECTION_R_PREVIOUS_SECTION_END_TIME_COL = 17;
    static final int SECTION_R_PREVIOUS_SECTION_END_DATE_COL = 18;
    static final int SECTION_R_PREVIOUS_DRIVE_ID_COL = 19;
    static final int SECTION_R_SECTION_TOTAL_TIME_COL = 20;
    static final int SECTION_R_SECTION_END_TIME_COL = 21;
    static final int SECTION_R_SECTION_START_TIME_COL = 22;
    static final int SECTION_R_SECTION_END_DATE_COL = 23;
    static final int SECTION_R_SECTION_START_DATE_COL = 24;
    static final int SECTION_R_TIME_COST_CODE_COL = 25;
    static final int SECTION_R_CAR_ID_COL = 27;
    static final int SECTION_R_CAR_POLUTION_RATE_COL = 29;
    static final int SECTION_R_SECTION_COST_COL = 30;
    static final int SECTION_R_RATE_CODE_COL = 31;
    static final int SECTION_R_SECTION_CODE_COL = 34;

    static int sectionCountRowsWithErr = 0;
    static int sectionCountRows = 0;

    static final boolean[] SECTION_R_CAN_ZERO_TIME =
            // A     B     C     D     E     F     G     H     I     J     K     L     M     N     O     P     Q     R     S     T     U     V     W     X     Y     Z    AA    AB    AC    AD    AE    AF    AG    AH    AI    AJ
            {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true ,true ,true ,false,false,false,false,false,false,false,false,false,false,false,false,false};
    static final boolean[] SECTION_R_CAN_ZERO =	{false,false,true ,true ,true ,false,false,false,false,false,true ,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true ,false,false,true ,true ,true ,false,false,false,false};
    static final boolean[] SECTION_R_CAN_NULL =	{false,false,false,false,false,false,false,false,false,false,false,true ,true ,false,false,false,false,true ,true ,true ,false,false,false,false,false,false,false,false,true ,false,false,false,true ,true ,false,false};
    //	static String[] Col_Names = {"סטאטוס מתנדב ","האם המתנדב בתקופת תיעוד ","תעריף זיכוי לקארפול ","כמות האנשים ברכב ","תשלום בגין המקטע ","קילומטראז' רכב סיום מקטע ","קילומטראז' רכב תחילת מקטע ","קוד הנסיעה לה משויך המקטע ","קוד מקטע ","מקטע ראשון של הנסיעה ","אורך מקטע הנסיעה ","קו רוחב סיום מקטע קודם ","קו אורך סיום מקטע קודם ","קו רוחב סיום מקטע ","קו אורך סיום מקטע ","קו רוחב תחילת מקטע ","קו אורך תחילת מקטע ","זמן סיום מקטע קודם ","תאריך סיום מקטע קודם ","קוד נסיעה של מקטע קודם ","משך נסיעה כולל למקטע ","זמן סיום המקטע ","זמן התחלת המקטע ","תאריך סיום מקטע ","תאריך תחילת מקטע ","קוד תעריף שעה ","קוד תעריף קטע דרך","קוד רכב אצל המנטר ","קידוד סוג הרכב ","שינוי התעריף עקב רמת הזיהום ","עלות לקילומטר מקטע ","קוד תעריף המקטע ","קוד רמת זיהום רכב קבוצה ","קוד רמת זיהום רכב ","קוד קטע דרך","קוד מתנדב ב VIA "};

    static final int PRIVATE_R_OUTPUT_CAR_ID_COL = 0;
    static final int PRIVATE_R_OUTPUT_DRIVE_ID_COL = 1;
    static final int PRIVATE_R_OUTPUT_TEST01_COL = 2;
    static final int PRIVATE_R_OUTPUT_TEST02_COL = 3;
    static final int PRIVATE_R_OUTPUT_TEST03_COL = 4;
    static final int PRIVATE_R_OUTPUT_TEST04_COL = 5;
    static final int PRIVATE_R_OUTPUT_TEST05_COL = 6;
    static final int PRIVATE_R_OUTPUT_TEST06_COL = 7;
    static final int PRIVATE_R_OUTPUT_TEST07_COL = 8;
    static final int PRIVATE_R_OUTPUT_TEST08_COL = 9;
    static final int PRIVATE_R_OUTPUT_TEST09_COL = 10;
    static final int PRIVATE_R_OUTPUT_TEST10_COL = 11;
    static final int PRIVATE_R_OUTPUT_TEST11_COL = 12;
    static final int PRIVATE_R_OUTPUT_TEST12_COL = 13;
    static final int PRIVATE_R_OUTPUT_TEST13_COL = 14;
    static final int PRIVATE_R_OUTPUT_TEST14_COL = 15;
    static final int PRIVATE_R_OUTPUT_TEST15_COL = 16;
    static final int PRIVATE_R_OUTPUT_ALL_TESTS_COL = 17;

    static List<String> privateColNamesList;
    static final int PRIVATE_R_ACTUAL_PAYMENT_COL = 0;
    static final int PRIVATE_R_POLLUTION_TOTAL_COST_COL = 3;
    static final int PRIVATE_R_POLLUTION_DIF_COL = 4;
    static final int PRIVATE_R_INITIAL_COST_COL = 5;
    static final int PRIVATE_R_FIRST_FOUR_COL = 6;
    static final int PRIVATE_R_PREVIOUS_DRIVE_CODE_COL = 7;
    static final int PRIVATE_R_DRIVE_ID_COL = 8;
    static final int PRIVATE_R_DRIVE_TOTAL_TIME_COL = 9;
    static final int PRIVATE_R_DRIVE_LENGTH_COL = 10;
    static final int PRIVATE_R_PREVIOUS_DRIVE_END_LATITUDE_COL = 13;
    static final int PRIVATE_R_PREVIOUS_DRIVE_END_LONGITUDE_COL = 14;
    static final int PRIVATE_R_DRIVE_END_LATITUDE_COL = 15;
    static final int PRIVATE_R_DRIVE_END_LONGITUDE_COL = 16;
    static final int PRIVATE_R_DRIVE_START_LATITUDE_COL = 17;
    static final int PRIVATE_R_DRIVE_START_LONGITUDE_COL = 18;
    static final int PRIVATE_R_PREVIOUS_END_TIME_COL = 19;
    static final int PRIVATE_R_PREVIOUS_END_DATE_COL = 20;
    static final int PRIVATE_R_END_TIME_COL = 21;
    static final int PRIVATE_R_END_DATE_COL = 22;
    static final int PRIVATE_R_START_TIME_COL = 23;
    static final int PRIVATE_R_START_DATE_COL = 24;
    static final int PRIVATE_R_CAR_ID_COL = 28;

    static int privateCountMissingRows = 0;
    static int privateCountRowsWithErr = 0;
    static int privateCountRows = 0;

    static final boolean[] PRIVATE_R_CAN_ZERO_TIME =
            // A     B     C     D     E     F     G     H     I     J     K     L     M     N     O     P     Q     R     S     T     U     V     W     X     Y     Z    AA    AB    AC    AD    AE    AF
            {false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,true ,false,false,false,false,false,false,false,false};
    static final boolean[] PRIVATE_R_CAN_NULL =	{false,false,false,false,false,false,false,true ,false,false,false,false,false,true ,true ,false,false,false,false,true ,true ,false,false,false,false,true ,true ,true ,false,false,false,false};
    static final boolean[] PRIVATE_R_CAN_ZERO =	{true ,true ,true ,true ,true ,true ,false,false,false,true ,true ,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false,false};



    public static void main(String[] args)  throws IOException, ParseException{

//		String str = getFieldsIndicators("private-travel-motorola-Indicated.xls", "yes 0","00:00:00");
//		System.out.println(str);

        System.out.println(new Date());
        long start = new Date().getTime();
        HashMap<String,String> reports;
        String strSectionReport = "";
        String strPrivateReport = "";
        String strSectionVolumeReport = "";
        PrintWriter pwReport = null;
        PrintWriter pwSectionVolume = null;
        nullStrings.add("null".toLowerCase());
        nullStrings.add("-".toLowerCase());
        zeroTimeStrings.add("00:00:00".toLowerCase());
        zeroStrings.add("0".toLowerCase());
        zeroStrings.add("0.0".toLowerCase());
        zeroStrings.add("0.00".toLowerCase());
        zeroStrings.add("0.000".toLowerCase());

        rates.put("TLC0".trim(), 3);
        rates.put("TLS0".trim(), 2);
        rates.put("JRC0".trim(), 3);
        rates.put("JRS1".trim(), 2);
        rates.put("JRS2".trim(), 2);
        rates.put("BRC0".trim(), 3);
        rates.put("BRS0".trim(), 2);
        rates.put("HAC0".trim(), 3);
        rates.put("HAS0".trim(), 2);
        rates.put("SUB0".trim(), 1);
        rates.put("REST".trim(), 0);

        costsRush.put("TLC0".trim(), 1.5);
        costsRush.put("TLS0".trim(), 0.3);
        costsRush.put("JRC0".trim(), 1.5);
        costsRush.put("JRS1".trim(), 0.3);
        costsRush.put("JRS2".trim(), 0.3);
        costsRush.put("BRC0".trim(), 1.5);
        costsRush.put("BRS0".trim(), 0.3);
        costsRush.put("HAC0".trim(), 1.5);
        costsRush.put("HAS0".trim(), 0.3);
        costsRush.put("REST".trim(), 0.0);
        costsRush.put("SUB0".trim(), 0.0);

        costsLow.put("TLC0".trim(), 0.1);
        costsLow.put("TLS0".trim(), 0.0);
        costsLow.put("JRC0".trim(), 0.1);
        costsLow.put("JRS1".trim(), 0.0);
        costsLow.put("JRS2".trim(), 0.0);
        costsLow.put("BRC0".trim(), 0.1);
        costsLow.put("BRS0".trim(), 0.0);
        costsLow.put("HAC0".trim(), 0.1);
        costsLow.put("HAS0".trim(), 0.0);
        costsLow.put("REST".trim(), 0.0);
        costsLow.put("SUB0".trim(), 0.0);


//		setSectionReportColNames();

//		static final String SECTION_REPORT_INPUT_FILE = "section-report-motorola.xls";
//		static final String SECTION_REPORT_OUTPUT_FILE = "output_section.csv";
//		static final String SECTION_VOLUME_REPORT_OUTPUT_FILE = "output_section-volume.csv";
//		static final String PRIVATE_TRAVEL_INPUT_FILE = "private-travel-motorola.xls";
//		static final String PRIVATE_TRAVEL_OUTPUT_FILE = "output_private-travel.csv";
//		static final String REPORT_TEST_COUNT_OUTPUT_FILE = "output_test-count.csv";


        FileOutputStream fos = new FileOutputStream(new File(OUTPUT_FILE));
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet_TEST_LIST = workbook.createSheet(TEST_LIST_OUTPUT_SHEET);
        HSSFSheet sheet_TEST_COUNT = workbook.createSheet(REPORT_TEST_COUNT_OUTPUT_SHEET);
        HSSFSheet sheet_SECTION_VOLUME = workbook.createSheet(SECTION_VOLUME_REPORT_OUTPUT_SHEET);
        HSSFSheet sheet_SECTION_REPORT = workbook.createSheet(SECTION_REPORT_OUTPUT_SHEET);
        HSSFSheet sheet_PRIVATE_TRAVEL = workbook.createSheet(PRIVATE_TRAVEL_OUTPUT_SHEET);
        Row row;
        Cell cell;
        CellStyle cellStyle;

        for(int i = 0; i<TESTS.length;i++) {
            row = sheet_TEST_LIST.createRow(i);
            cell = row.createCell(0);
            cellStyle = cell.getCellStyle();
            cellStyle.setAlignment(HorizontalAlignment.RIGHT);
            cellStyle.setWrapText(true);
            cell.setCellValue(right2left_text + TESTS[i]);
            cell.setCellStyle(cellStyle);
        }
        sheet_TEST_LIST.autoSizeColumn(0);
        sheet_TEST_COUNT.autoSizeColumn(0);
        sheet_SECTION_VOLUME.autoSizeColumn(0);
        sheet_SECTION_REPORT.autoSizeColumn(0);
        sheet_PRIVATE_TRAVEL.autoSizeColumn(0);

        reports = getReports(SECTION_REPORT_INPUT_FILE,PRIVATE_TRAVEL_INPUT_FILE,sheet_PRIVATE_TRAVEL,sheet_SECTION_REPORT);

        strSectionReport = reports.get(SECTION_REPORT_INPUT_FILE);

        pwReport = new PrintWriter(new File(SECTION_REPORT_OUTPUT_SHEET + CSV));
//		System.out.println(strSectionReport);

        double errRowPresent = 100.0;
        errRowPresent = (errRowPresent*sectionCountRowsWithErr)/sectionCountRows;
        String sectionReportHeaders = sectionColNamesList.get(SECTION_R_CAR_ID_COL).trim() + "," +
                sectionColNamesList.get(SECTION_R_DRIVE_ID_COL).trim() + "," +
                sectionColNamesList.get(SECTION_R_SECTION_ID_COL).trim() + "," + "תקלה/תקלות" + "," +
                sectionCountRowsWithErr + " Rows with errors found," + sectionCountRows + " Rows checked," +
                errRowPresent + " Rows error present";

        row = sheet_SECTION_REPORT.createRow(0);
        cell = row.createCell(SECTION_R_OUTPUT_CAR_ID_COL);
        cell.setCellValue(right2left_text + sectionColNamesList.get(SECTION_R_CAR_ID_COL).trim());
        cell = row.createCell(SECTION_R_OUTPUT_DRIVE_ID_COL);
        cell.setCellValue(right2left_text + sectionColNamesList.get(SECTION_R_DRIVE_ID_COL).trim());
        cell = row.createCell(SECTION_R_OUTPUT_SECTION_ID_COL);
        cell.setCellValue(right2left_text + sectionColNamesList.get(SECTION_R_SECTION_ID_COL).trim());
        for(int i = SECTION_R_OUTPUT_TEST01_COL;i<SECTION_R_OUTPUT_ALL_TESTS_COL;i++) {
            cell = row.createCell(i);
            cell.setCellValue(left2right_text + "Test-" + (i+1-SECTION_R_OUTPUT_TEST01_COL));
        }
        cell = row.createCell(SECTION_R_OUTPUT_ALL_TESTS_COL);
        cell.setCellValue(right2left_text + "תקלה/תקלות");
        cell = row.createCell(SECTION_R_OUTPUT_ALL_TESTS_COL+1);
        cell.setCellValue(left2right_text + sectionCountRowsWithErr + " Rows with errors found");
        cell = row.createCell(SECTION_R_OUTPUT_ALL_TESTS_COL+2);
        cell.setCellValue(left2right_text + sectionCountRows + " Rows checked");
        cell = row.createCell(SECTION_R_OUTPUT_ALL_TESTS_COL+3);
        cell.setCellValue(left2right_text + errRowPresent + " Rows error present");

        pwReport.append(sectionReportHeaders + "\n" + strSectionReport);
        closePrintWriter(pwReport, SECTION_REPORT_OUTPUT_SHEET + CSV);


        pwSectionVolume = new PrintWriter(new File(SECTION_VOLUME_REPORT_OUTPUT_SHEET + CSV));
        strSectionVolumeReport = "";


        SortedSet<String> treeKeySet = new TreeSet<>();
        treeKeySet.addAll(sectionsPerDay.keySet());
        treeKeySet.addAll(peoplePerDay.keySet());
        treeKeySet.addAll(drivesPerDay.keySet());
        treeKeySet.addAll(errorsPerDay.keySet());


        List<String> keyList = new LinkedList<String>(treeKeySet);

        boolean change = true;
        for(int i = 0;(i<keyList.size())&&(change);i++) {
            change = false;
            for(int j = 0; j<keyList.size() - i - 2;j++) {
                SimpleDateFormat myDate = new SimpleDateFormat("dd/MM/yyyy");
                myDate.setTimeZone(TimeZone.getTimeZone("GMT+3"));
                Date date = myDate.parse(keyList.get(j));
                Date date2 = myDate.parse(keyList.get(j+1));
                if(date.compareTo(date2) > 0) {
                    String temp = keyList.get(j);
                    keyList.set(j,keyList.get(j+1));
                    keyList.set(j+1,temp);
                    change = true;
                }
            }
        }

        int countErr = 0;
        Set<String> errKeys = errorsPerDay.keySet();

        for(String key: errKeys) {
            countErr += errorsPerDay.get(key);
        }
        for(int i = 0 ; i < keyList.size() ; i++) {
            String key = keyList.get(i);
            row = sheet_SECTION_VOLUME.createRow(i+1);
            cell = row.createCell(0);
            cell.setCellValue(key);
            strSectionVolumeReport += key + ",";
            cell = row.createCell(1);
            cell.setCellValue(peoplePerDay.get(key).size() + "");
            strSectionVolumeReport += peoplePerDay.get(key).size() + ",";
            cell = row.createCell(2);
            cell.setCellValue(sectionsPerDay.get(key).size() + "");
            strSectionVolumeReport += sectionsPerDay.get(key).size() + ",";
            cell = row.createCell(3);
            cell.setCellValue(drivesPerDay.get(key).size() + "");
            strSectionVolumeReport += drivesPerDay.get(key).size() + ",";
            cell = row.createCell(4);
            cell.setCellValue(errorsPerDay.get(key) + "");
            strSectionVolumeReport +=  errorsPerDay.get(key) + "\n";
        }

        String sectionVolumeReportHeaders = "תאריך,כמות מכוניות, כמות מקטעים,מספר נסיעות,כמות תקלות " + "," +
                sectionCountRowsWithErr + " Rows with errors found," +
                sectionCountRows + " Rows checked," +
                errRowPresent + " Rows error present," +
                countErr + " errors found";

        row = sheet_SECTION_VOLUME.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue(right2left_text + "תאריך");
        cell = row.createCell(1);
        cell.setCellValue(right2left_text + "כמות מכוניות");
        cell = row.createCell(2);
        cell.setCellValue(right2left_text + "כמות מקטעים");
        cell = row.createCell(3);
        cell.setCellValue(right2left_text + "מספר נסיעות");
        cell = row.createCell(4);
        cell.setCellValue(right2left_text + "כמות תקלות");
        cell = row.createCell(5);
        cell.setCellValue(left2right_text + sectionCountRowsWithErr + " Rows with errors found");
        cell = row.createCell(6);
        cell.setCellValue(left2right_text + sectionCountRows + " Rows checked");
        cell = row.createCell(7);
        cell.setCellValue(left2right_text + errRowPresent + " Rows error present");
        cell = row.createCell(8);
        cell.setCellValue(left2right_text + countErr + " errors found");

//		System.out.println(strSectionVolumeReport);
        pwSectionVolume.append(sectionVolumeReportHeaders  + "\n" + strSectionVolumeReport);
        closePrintWriter(pwSectionVolume,SECTION_VOLUME_REPORT_OUTPUT_SHEET + CSV);


        double errRowPresent2 = 100.0;
        errRowPresent2 = (errRowPresent2*privateCountRowsWithErr)/privateCountRows;

        strPrivateReport = reports.get(PRIVATE_TRAVEL_INPUT_FILE);
        String privateReportHeaders =  sectionColNamesList.get(SECTION_R_CAR_ID_COL).trim() + "," +
                sectionColNamesList.get(SECTION_R_DRIVE_ID_COL).trim() + "," + "תקלה/תקלות," +
                privateCountRowsWithErr + " Rows with errors, " +
                privateCountRows + " Rows checked, " +
                privateCountMissingRows + " Rows missing, " +
                errRowPresent2 + " Rows error present";
        row = sheet_PRIVATE_TRAVEL.createRow(0);
        cell = row.createCell(PRIVATE_R_OUTPUT_CAR_ID_COL);
        cell.setCellValue(right2left_text + sectionColNamesList.get(SECTION_R_CAR_ID_COL).trim());
        cell = row.createCell(PRIVATE_R_OUTPUT_DRIVE_ID_COL);
        cell.setCellValue(right2left_text + sectionColNamesList.get(SECTION_R_DRIVE_ID_COL).trim());
        for(int i = PRIVATE_R_OUTPUT_TEST01_COL;i<PRIVATE_R_OUTPUT_ALL_TESTS_COL;i++) {
            cell = row.createCell(i);
            cell.setCellValue(left2right_text + "Test-" + (i+1-PRIVATE_R_OUTPUT_TEST01_COL));
        }
        cell = row.createCell(PRIVATE_R_OUTPUT_ALL_TESTS_COL);
        cell.setCellValue(right2left_text + "תקלה/תקלות");
        cell = row.createCell(PRIVATE_R_OUTPUT_ALL_TESTS_COL+1);
        cell.setCellValue(left2right_text + privateCountRowsWithErr + " Rows with errors");
        cell = row.createCell(PRIVATE_R_OUTPUT_ALL_TESTS_COL+2);
        cell.setCellValue(left2right_text + privateCountRows + " Rows checked");
        cell = row.createCell(PRIVATE_R_OUTPUT_ALL_TESTS_COL+3);
        cell.setCellValue(left2right_text + privateCountMissingRows + " Rows missing");
        cell = row.createCell(PRIVATE_R_OUTPUT_ALL_TESTS_COL+4);
        cell.setCellValue(left2right_text + errRowPresent2 + " Rows error present");

        pwReport = new PrintWriter(new File(PRIVATE_TRAVEL_OUTPUT_SHEET + CSV));
        pwReport.append(privateReportHeaders + "\n" + strPrivateReport);
        closePrintWriter(pwReport,PRIVATE_TRAVEL_OUTPUT_SHEET + CSV);


        String strTestCount = "Section Test No.,Errors found," + sectionCountRows + " Rows checked, " + "Private Test No., Errors found, " + privateCountRows + " Rows checked" + "\n";
        row = sheet_TEST_COUNT.createRow(0);
        cell = row.createCell(0);
        cell.setCellValue(left2right_text + "Section Test No.");
        cell = row.createCell(1);
        cell.setCellValue(left2right_text + "Errors found");
        cell = row.createCell(2);
        cell.setCellValue(left2right_text + sectionCountRows + " Rows checked");
        cell = row.createCell(3);
        cell.setCellValue(left2right_text + "Private Test No.");
        cell = row.createCell(4);
        cell.setCellValue(left2right_text + "Errors found");
        cell = row.createCell(5);
        cell.setCellValue(left2right_text + privateCountRows + " Rows checked");

        int rowIndicator = 1;
        for(int i = 0; ((i < sectionTestCount.length)||(i < privateTestCount.length));i++) {
            int startRowIndicator = rowIndicator;
            String subTestCount = "";
            row = sheet_TEST_COUNT.createRow(rowIndicator);

            if(i<sectionTestCount.length) {
                if(sectionTestCount[i]>0) {
                    subTestCount += "" +(i+1) + "," + sectionTestCount[i] +","+",";
                    cell = row.createCell(0);
                    cell.setCellValue(left2right_text + "" +(i+1));
                    cell = row.createCell(1);
                    cell.setCellValue(left2right_text + sectionTestCount[i] + "");

                    rowIndicator++;
                }
                else
                    subTestCount += "" + "," +","+",";
            }
            else
                subTestCount += "" + "," +","+",";

            if(i<privateTestCount.length) {
                if(privateTestCount[i]>0) {
                    subTestCount += "" +(i+1) + "," + privateTestCount[i] +",";
                    cell = row.createCell(3);
                    cell.setCellValue(left2right_text + "" + (i+1));
                    cell = row.createCell(4);
                    cell.setCellValue(left2right_text + privateTestCount[i] +"");
                    if(rowIndicator == startRowIndicator) {
                        rowIndicator++;
                    }
                }
                else
                    subTestCount += "" + "," +",";
            }
            else
                subTestCount += "" + "," +",";

            if(subTestCount.contains((i+1) + ""))
                strTestCount += subTestCount + "\n";
        }

        int i;
        row = sheet_TEST_LIST.getRow(0);
        cell = row.createCell(1);
        cell.setCellValue(left2right_text + sectionCountRowsWithErr + " Rows with errors");
        cell = row.createCell(2);
        cell.setCellValue(left2right_text + sectionCountRows + " Rows checked");
        cell = row.createCell(3);
        cell.setCellValue(left2right_text + (((int)((((100.0*sectionCountRowsWithErr)/sectionCountRows)*10000)))/10000.0) + " Rows error present");

        for(i = 0; i<sectionTestCount.length;i++) {
            row = sheet_TEST_LIST.getRow(i+2);
            cell = row.createCell(1);
            cell.setCellValue(left2right_text + sectionTestCount[i] + "");
        }
        int startI = i + 3;
        row = sheet_TEST_LIST.getRow(startI);
        cell = row.createCell(1);
        cell.setCellValue(left2right_text + privateCountRowsWithErr + " Rows with errors");
        cell = row.createCell(2);
        cell.setCellValue(left2right_text + privateCountRows + " Rows checked");
        cell = row.createCell(3);
        cell.setCellValue(left2right_text + (((int)((((100.0*privateCountRowsWithErr)/privateCountRows)*10000)))/10000.0) + " Rows error present");
        cell = row.createCell(4);
        cell.setCellValue(left2right_text + privateCountMissingRows + " Rows missing");

        for(i = 0; i<privateTestCount.length;i++) {
            row = sheet_TEST_LIST.getRow(i+1+startI);
//			if(row==null) {
//				row = sheet_TEST_LIST.createRow(i+1+startI);
//			}
            cell = row.createCell(1);
            cell.setCellValue(left2right_text + privateTestCount[i] + "");
        }


        pwReport = new PrintWriter(new File(REPORT_TEST_COUNT_OUTPUT_SHEET + CSV));
        pwReport.append(strTestCount);
        closePrintWriter(pwReport,REPORT_TEST_COUNT_OUTPUT_SHEET + CSV);


        workbook.write(fos);
        fos.close();
        workbook.close();

        System.out.println("Generating the reports took " + (new Date().getTime() - start)/1000.0 + " seconds");
        System.out.println("Section Report: " + sectionCountRowsWithErr + " Rows with errors, " + sectionCountRows + " Rows checked, " + errRowPresent + " Rows error present, " + countErr + " Errors found");
        System.out.println("Private Report: " + privateCountRowsWithErr + " Rows with errors, " + privateCountRows + " Rows checked, " + privateCountMissingRows + " Rows missing, " + errRowPresent2 + " Rows error present" );
        System.out.println("Test Report: \n" + strTestCount);


        //		for(int i = 0; (i<Col_Names.length)&&(i<colNamesList.size());i++) {
        //			if(Col_Names[i].trim().equalsIgnoreCase(colNamesList.get(i).trim())) {
        //				System.out.println(colNamesList.get(i));
        //			}
        //			else {
        //				System.out.println(colNamesList.get(i) + "\t" + Col_Names[i]);
        //			}
        //
        //		}

        //		for(Entry<String, List<Row>> entry:sectionMap.entrySet()) {
        //			String str = entry.getKey();
        //			for(int i = 0; i<entry.getValue().size();i++) {
        //				str += "\n" + (i+1) + ") ";
        //				for(int j = 0; j<entry.getValue().get(i).getLastCellNum();j++) {
        //					try {
        //					str += "(" +(j+1) + ") " + entry.getValue().get(i).getCell(j).getStringCellValue() + "\t";
        //					}
        //					catch (Exception e) {
        //					}
        //				}
        //			}
        //			System.out.println(str);
        //		}

    }

    //	private static void setSectionReportColNames() throws IOException {
    //		HashMap<String, List<Row>> sectionMap = new HashMap<>();
    //		FileInputStream fisSection = new FileInputStream(new File("section-report-motorola.xls"));
    //		HSSFWorkbook workbookSection = new HSSFWorkbook(fisSection);
    //		HSSFSheet sheetSection = workbookSection.getSheetAt(0);
    //		FormulaEvaluator formulaEvaluator = workbookSection.getCreationHelper().createFormulaEvaluator();
    //
    //		colNamesList = new LinkedList<>();
    //		Row row0 = sheetSection.getRow(0);
    //		for(int i = 0;i<row0.getLastCellNum();i++) {
    //			colNamesList.add(row0.getCell(i).getStringCellValue() + "");
    //		}
    //	}

    @SuppressWarnings("deprecation")
    private static HashMap<String,String> getReports(String sectionReportFileName, String privateReportFileName,HSSFSheet PRIVATE_TRAVEL,HSSFSheet SECTION_REPORT)  throws IOException, ParseException{

        HashMap<String, List<Row>> sectionMap = new HashMap<>();
        HashMap<String, List<Row>> privateMap = new HashMap<>();
        FileInputStream fis = new FileInputStream(new File(sectionReportFileName));
        HSSFWorkbook workbook = new HSSFWorkbook(fis);
        HSSFSheet sheet = workbook.getSheetAt(0);
        FormulaEvaluator formulaEvaluatorSection = workbook.getCreationHelper().createFormulaEvaluator();
        DataFormatter objDefaultFormat = new DataFormatter();
        FormulaEvaluator objFormulaEvaluator = new HSSFFormulaEvaluator(workbook);
        int PrivateTravelRowCount = 1,
                SectionReportRowCount = 1;



        sectionColNamesList = new LinkedList<>();
        Row row0 = sheet.getRow(0);
        for(int i = 0;i<row0.getLastCellNum();i++) {
            sectionColNamesList.add(row0.getCell(i).getStringCellValue().trim() + "");
        }


        //Divide the Section Report rows by CAR_ID
        for(int i = 1;i<sheet.getLastRowNum();i++) {
            Row row = sheet.getRow(i);
            String id = row.getCell(SECTION_R_CAR_ID_COL).getStringCellValue().trim()+
                    KEY_SEPERATOR + row.getCell(SECTION_R_DRIVE_ID_COL).getStringCellValue().trim();
            if(sectionMap.get(id) == null) {
                sectionMap.put(id, new LinkedList<>());
            }
            sectionMap.get(id).add(row);

//			Cell cellZero = row.getCell(SECTION_START_DATE_COL);
//			String strStartDateZero = getStringFromCol(cellZero, formulaEvaluator).trim();

        }
        workbook.close();

        fis = new FileInputStream(new File(privateReportFileName));
        workbook = new HSSFWorkbook(fis);
        sheet = workbook.getSheetAt(0);
        FormulaEvaluator formulaEvaluatorPrivate = workbook.getCreationHelper().createFormulaEvaluator();

        privateColNamesList = new LinkedList<>();
        row0 = sheet.getRow(0);
        for(int i = 0;i<row0.getLastCellNum();i++) {
            privateColNamesList.add(row0.getCell(i).getStringCellValue().trim() + "");
        }
        //Divide the Private Travel rows by CAR_ID and DRIVE_ID
        List<Row> privateRowList = new LinkedList<Row>();
        for(int i = 1;i<sheet.getLastRowNum();i++) {
            Row row = sheet.getRow(i);
            String id = row.getCell(PRIVATE_R_CAR_ID_COL).getStringCellValue().trim()+
                    KEY_SEPERATOR + row.getCell(PRIVATE_R_DRIVE_ID_COL).getStringCellValue().trim();
            if(privateMap.get(id) == null) {
                privateMap.put(id, new LinkedList<>());
            }
            privateMap.get(id).add(row);
            privateRowList.add(row);
//			Cell cellZero = row.getCell(SECTION_START_DATE_COL);
//			String strStartDateZero = getStringFromCol(cellZero, formulaEvaluator).trim();

        }
        HashMap<String, List<Row>> sectionsCodes = new HashMap<>();
        HashMap<String, List<Row>> drivesCodes = new HashMap<>();

        Collections.sort(privateRowList,new Comparator<Row>() {
            @Override
            public int compare(Row r1, Row r2) {
                Cell c1 = r1.getCell(PRIVATE_R_DRIVE_ID_COL);
                objFormulaEvaluator.evaluate(c1);
                String id1 = objDefaultFormat.formatCellValue(c1,objFormulaEvaluator).trim();
                Cell c2 = r2.getCell(PRIVATE_R_DRIVE_ID_COL);
                objFormulaEvaluator.evaluate(c2);
                String id2 = objDefaultFormat.formatCellValue(c2,objFormulaEvaluator).trim();

                return id1.compareToIgnoreCase(id2);
            }
        });
//		for(Entry<String, List<Row>> entry:privateMap.entrySet())
//			System.out.println(entry);

        workbook.close();



        String strSectionReport = "";
        String strPrivateReport = "";

        List<Entry<String, List<Row>>> sortedEntryList = new LinkedList<>(sectionMap.entrySet());
        Collections.sort(sortedEntryList,new Comparator<Entry<String, List<Row>>>() {
            @Override
            public int compare(Entry<String, List<Row>> o1, Entry<String, List<Row>> o2) {
                return o1.getKey().compareTo(o2.getKey());
            }
        });
        for(Entry<String, List<Row>> entry: sortedEntryList)
//			if(!entry.getKey().trim().contains("df5ab45b-bc0d-49b6-bacd-c62deb028840".trim()))
        {


            for(int i = 0; i<entry.getValue().size();i++) {
                Row rowSection = entry.getValue().get(i);
                Cell cellZero = rowSection.getCell(SECTION_R_SECTION_START_DATE_COL);
                String strStartDateZero = getStringFromCol(cellZero, formulaEvaluatorSection).trim();
//				if(!strStartDateZero.contains("27")){
//					break;
//				}
                sectionCountRows++;
                if(errorsPerDay.get(strStartDateZero) == null) {
                    errorsPerDay.put(strStartDateZero, 0);
                }
                //add row indicator
                String strSubSectionReportIndicators = //"Row No." + (i+1) + "," +
                        rowSection.getCell(SECTION_R_CAR_ID_COL).getStringCellValue().trim() + "," +
                                rowSection.getCell(SECTION_R_DRIVE_ID_COL).getStringCellValue().trim() + "," +
                                rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim() + ",";
                String strSubSectionReport = "";
                Row sectionOutputRow = SECTION_REPORT.createRow(SectionReportRowCount);
                for(int j = SECTION_R_OUTPUT_TEST01_COL; j<SECTION_R_OUTPUT_ALL_TESTS_COL+1;j++) {
                    sectionOutputRow.createCell(j);
                }

                if(rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim().contains("ef153ddc-fc2b-40e8-b6a3-10fb29ef8004-1")) {
                    System.out.println();
                }

//S---test-01---:cells values check Null, 0 and 00:00:00.
                for(int j = 0; j<rowSection.getLastCellNum();j++) {
                    Cell cell = rowSection.getCell(j);
                    String str = getStringFromCol(cell, formulaEvaluatorSection);
                    if(formulaEvaluatorSection.evaluateInCell(cell).getCellType() == Cell.CELL_TYPE_BLANK) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);

                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(j).trim() + " לא יכולה יהיות ריקה ",sectionOutputRow, objFormulaEvaluator);

                    }

                    if(null != str) {
                        if(nullStrings.contains(str.toLowerCase().trim())&&!SECTION_R_CAN_NULL[j]) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-01: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(j).trim() + " לא יכול להיות 'NULL' ",sectionOutputRow, objFormulaEvaluator);
                        }
                        if(zeroStrings.contains(str.toLowerCase())&&!SECTION_R_CAN_ZERO[j]) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-01: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(j).trim() + " לא יכול להיות '0' ",sectionOutputRow, objFormulaEvaluator);
                        }
                        if(zeroTimeStrings.contains(str.toLowerCase().trim())&&!SECTION_R_CAN_ZERO_TIME[j]) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-01: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(j).trim() + " לא יכול להיות '00:00:00' ",sectionOutputRow, objFormulaEvaluator);
                        }
                    }
                }

//S---test-02---:check if there is a previous section code
                Cell cellCurrent = rowSection.getCell(SECTION_R_PREVIOUS_DRIVE_ID_COL);
                String strPreDrive = getStringFromCol(cellCurrent, formulaEvaluatorSection);

                if(!nullStrings.contains(strPreDrive.toLowerCase())) {

                    Row rowPre = null;
                    for(int j = 0;(j<entry.getValue().size())&&(null == rowPre);j++) {
                        rowPre = entry.getValue().get(j);
                        Cell cellDrive = rowPre.getCell(SECTION_R_SECTION_ID_COL);
                        String strDrive = getStringFromCol(cellDrive, formulaEvaluatorSection);
                        if(!strDrive.trim().equalsIgnoreCase(strPreDrive.trim())) {
                            rowPre = null;
                        }
                    }

                    if(null == rowPre) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-02: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST02_COL," יש " + sectionColNamesList.get(SECTION_R_PREVIOUS_DRIVE_ID_COL).trim() + " " + strPreDrive + " אבל אין " +
                                        sectionColNamesList.get(SECTION_R_DRIVE_ID_COL).trim() + " בשורה אחרת של קוד הרכב הזה ",sectionOutputRow, objFormulaEvaluator);
                    }
//S---test-03-------:check if all relevant values are the same between rows
                    else {
                        //Latitude check
                        cellCurrent = rowSection.getCell(SECTION_R_PREVIOUS_SECTION_END_LATITUDE_COL);
                        String strPreEndLatitude = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                        if(nullStrings.contains(strPreEndLatitude.toLowerCase().trim())) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-02: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST02_COL,sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_LATITUDE_COL).trim() + " לא יכל להיות 'NULL'-אם "
                                            + sectionColNamesList.get(SECTION_R_PREVIOUS_DRIVE_ID_COL).trim() + " לא 'NULL' ",sectionOutputRow, objFormulaEvaluator);
                            ;
                        }
                        else {
                            cellCurrent = rowSection.getCell(SECTION_R_SECTION_START_LATITUDE_COL);
                            String strStartLatitude = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                            Cell cellPre = rowPre.getCell(SECTION_R_SECTION_END_LATITUDE_COL);
                            String strPreEndLatitude2 = getStringFromCol(cellPre, formulaEvaluatorSection);
                            if(!threeStringEqualsValidation(strStartLatitude,strPreEndLatitude2,strPreEndLatitude)) {
                                errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                strSubSectionReport += "Test-03: " +
                                        addToSectionOutput(SECTION_R_OUTPUT_TEST03_COL,sectionColNamesList.get(SECTION_R_SECTION_END_LATITUDE_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_LATITUDE_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_SECTION_START_LATITUDE_COL).trim() +" לא שווים ",sectionOutputRow, objFormulaEvaluator);;
                            }
                        }
                        //Longitude check
                        cellCurrent = rowSection.getCell(SECTION_R_PREVIOUS_SECTION_END_LONGITUDE_COL);
                        String strPreEndLongitude = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                        if(nullStrings.contains(strPreEndLongitude.toLowerCase().trim())) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-02: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST02_COL,sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_LONGITUDE_COL).trim() + " לא יכל להיות 'NULL'-אם "
                                            + sectionColNamesList.get(SECTION_R_PREVIOUS_DRIVE_ID_COL).trim() + " לא 'NULL' ",sectionOutputRow, objFormulaEvaluator);
                        }
                        else {
                            cellCurrent = rowSection.getCell(SECTION_R_SECTION_START_LONGITUDE_COL);
                            String strStartLongitude = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                            Cell cellPre = rowPre.getCell(SECTION_R_SECTION_END_LONGITUDE_COL);
                            String strPreEndLongitude2 = getStringFromCol(cellPre, formulaEvaluatorSection);
                            if(!threeStringEqualsValidation(strStartLongitude,strPreEndLongitude2,strPreEndLongitude)) {
                                errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                strSubSectionReport += "Test-03: " +
                                        addToSectionOutput(SECTION_R_OUTPUT_TEST03_COL,sectionColNamesList.get(SECTION_R_SECTION_END_LONGITUDE_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_LONGITUDE_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_SECTION_START_LONGITUDE_COL).trim() +" לא שווים ",sectionOutputRow, objFormulaEvaluator);
                            }
                        }

                        //Time check
                        cellCurrent = rowSection.getCell(SECTION_R_PREVIOUS_SECTION_END_TIME_COL);
                        String strPreEndTime = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                        if(nullStrings.contains(strPreEndTime.toLowerCase().trim())) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-02: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST02_COL,sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_TIME_COL).trim() + " לא יכל להיות 'NULL'-אם "
                                            + sectionColNamesList.get(SECTION_R_PREVIOUS_DRIVE_ID_COL).trim() + " לא 'NULL' ",sectionOutputRow, objFormulaEvaluator);;
                        }
                        else {
                            cellCurrent = rowSection.getCell(SECTION_R_SECTION_START_TIME_COL);
                            String strStartTime = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                            Cell cellPre = rowPre.getCell(SECTION_R_SECTION_END_TIME_COL);
                            String strPreEndTime2 = getStringFromCol(cellPre, formulaEvaluatorSection);
                            if(!threeStringEqualsValidation(strStartTime,strPreEndTime2,strPreEndTime)) {
                                errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                strSubSectionReport += "Test-03: " +
                                        addToSectionOutput(SECTION_R_OUTPUT_TEST03_COL,sectionColNamesList.get(SECTION_R_SECTION_END_TIME_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_TIME_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_SECTION_START_TIME_COL).trim() +" לא שווים ",sectionOutputRow, objFormulaEvaluator);
                            }
                        }

                        //Date check
                        cellCurrent = rowSection.getCell(SECTION_R_PREVIOUS_SECTION_END_DATE_COL);
                        String strPreEndDate = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                        if(nullStrings.contains(strPreEndDate.toLowerCase().trim())) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-02: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST02_COL,sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_DATE_COL).trim() + " לא יכל להיות 'NULL'-אם "
                                            + sectionColNamesList.get(SECTION_R_PREVIOUS_DRIVE_ID_COL).trim() + " לא 'NULL' ",sectionOutputRow, objFormulaEvaluator);;
                        }
                        else {
                            cellCurrent = rowSection.getCell(SECTION_R_SECTION_START_DATE_COL);
                            String strStartDate = getStringFromCol(cellCurrent, formulaEvaluatorSection);
                            Cell cellPre = rowPre.getCell(SECTION_R_SECTION_END_DATE_COL);
                            String strPreEndDate2 = getStringFromCol(cellPre, formulaEvaluatorSection);
                            if(!threeStringEqualsValidation(strStartDate,strPreEndDate2,strPreEndDate)) {
                                errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                strSubSectionReport += "Test-03: " +
                                        addToSectionOutput(SECTION_R_OUTPUT_TEST03_COL,sectionColNamesList.get(SECTION_R_SECTION_END_DATE_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_PREVIOUS_SECTION_END_DATE_COL).trim() + "-" +
                                                sectionColNamesList.get(SECTION_R_SECTION_START_DATE_COL).trim() +" לא שווים ",sectionOutputRow, objFormulaEvaluator);;
                            }
                        }
                    }
//S---test-07------:if there is a previous section code check to see if this section is marked as the first section
                    if(getStringFromCol(rowSection.getCell(SECTION_R_FIRST_SECTION_COL),formulaEvaluatorSection).trim().equalsIgnoreCase("yes".trim())) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-07: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST07_COL,sectionColNamesList.get(SECTION_R_FIRST_SECTION_COL).trim() + " שווה ל " +
                                        getStringFromCol(rowSection.getCell(SECTION_R_FIRST_SECTION_COL),formulaEvaluatorSection).trim() + " ויש " +
                                        sectionColNamesList.get(SECTION_R_PREVIOUS_DRIVE_ID_COL).trim() + " שהוא " +
                                        getStringFromCol(rowSection.getCell(SECTION_R_PREVIOUS_DRIVE_ID_COL),formulaEvaluatorSection).trim() + " ",sectionOutputRow, objFormulaEvaluator);;
                    }
                }
                else {
//S---test-07-------:if there is not a previous section code check to see if this section is marked as the not first section
                    if(getStringFromCol(rowSection.getCell(SECTION_R_FIRST_SECTION_COL),formulaEvaluatorSection).trim().equalsIgnoreCase("no".trim())) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-07: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST07_COL,sectionColNamesList.get(SECTION_R_FIRST_SECTION_COL).trim() + " שווה ל " +
                                        getStringFromCol(rowSection.getCell(SECTION_R_FIRST_SECTION_COL),formulaEvaluatorSection).trim() + " ו" +
                                        sectionColNamesList.get(SECTION_R_PREVIOUS_DRIVE_ID_COL).trim()  + " הוא " +
                                        getStringFromCol(rowSection.getCell(SECTION_R_PREVIOUS_DRIVE_ID_COL),formulaEvaluatorSection).trim() + " ",sectionOutputRow, objFormulaEvaluator);;
                    }
                }


                cellCurrent = rowSection.getCell(SECTION_R_SECTION_LENGTH_COL);
                String strSectionLength = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                cellCurrent = rowSection.getCell(SECTION_R_SECTION_START_LENGTH_COL);
                String strSectionStartLength = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                cellCurrent = rowSection.getCell(SECTION_R_SECTION_END_LENGTH_COL);
                String strSectionEndLength = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();

                double sectionLength = -1;
                double sectionStartLength = -1;
                double sectionEndLength = -1;
                try {
                    sectionLength = Double.valueOf(strSectionLength.trim());
                    if(sectionLength<0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_LENGTH_COL).trim() + " שלילי ",sectionOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e){
                    if(sectionLength == -1)
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                    strSubSectionReport += "Test-01: " +
                            addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_LENGTH_COL).trim() + " הוא לא מספר " + strSectionLength + " ",sectionOutputRow, objFormulaEvaluator);
                }
                try {
                    sectionStartLength = Double.valueOf(strSectionStartLength.trim());
                    if(sectionStartLength<0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_START_LENGTH_COL).trim() + " שלילי ",sectionOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e){
                    if(sectionStartLength == -1)
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                    strSubSectionReport += "Test-01: " +
                            addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_START_LENGTH_COL).trim() + " הוא לא מספר " + strSectionStartLength + " ",sectionOutputRow, objFormulaEvaluator);
                }
                try {
                    sectionEndLength = Double.valueOf(strSectionEndLength.trim());
                    if(sectionEndLength<0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_END_LENGTH_COL).trim() + " שלילי ",sectionOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e){
                    if(sectionEndLength == -1)
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                    strSubSectionReport += "Test-01: " +
                            addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_END_LENGTH_COL).trim() + " הוא לא מספר " + strSectionEndLength + " ",sectionOutputRow, objFormulaEvaluator);
                }

//S---test-05---:check if kilometrage at the end minus kilometrage at the start equals to the distance traveled
                if((sectionLength!=-1)&&(sectionStartLength!=-1)&&(sectionEndLength!=-1)&&(!comparisonDoublesABS(sectionEndLength - sectionStartLength, sectionLength, maxErrorLength))) {
                    errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                    strSubSectionReport += "Test-05: " +
                            addToSectionOutput(SECTION_R_OUTPUT_TEST05_COL,sectionColNamesList.get(SECTION_R_SECTION_LENGTH_COL).trim() + " " + sectionLength +
                                    " לא שווה לחיסור בין " + sectionColNamesList.get(SECTION_R_SECTION_END_LENGTH_COL).trim() + " " + sectionEndLength +
                                    " ל- " + sectionColNamesList.get(SECTION_R_SECTION_START_LENGTH_COL).trim() +  " " + sectionStartLength +
                                    " " + sectionColNamesList.get(SECTION_R_SECTION_START_DATE_COL).trim() + " " +
                                    getStringFromCol(rowSection.getCell(SECTION_R_SECTION_START_DATE_COL), formulaEvaluatorSection).trim() + " ",sectionOutputRow, objFormulaEvaluator);
                }


                cellCurrent = rowSection.getCell(SECTION_R_SECTION_START_DATE_COL);
                String strStartDate = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                cellCurrent = rowSection.getCell(SECTION_R_SECTION_END_DATE_COL);
                String strEndDate = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                SimpleDateFormat sdfTime = new SimpleDateFormat("dd/MM/yyyy");
                double secondsBetween = -1;
                try {
                    secondsBetween = (sdfTime.parse(strEndDate).getTime()-sdfTime.parse(strStartDate).getTime())/1000;
                } catch (ParseException e1) {

                }

                cellCurrent = rowSection.getCell(SECTION_R_SECTION_TOTAL_TIME_COL);


                objFormulaEvaluator.evaluate(cellCurrent); // This will evaluate the cell, And any type of cell will return string value
                String strSectionTotalTime = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();

//				String strSectionTotalTime = getStringFromCol(cellCurrent, formulaEvaluator).trim();

                cellCurrent = rowSection.getCell(SECTION_R_SECTION_END_TIME_COL);
                String strSectionEndTime = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                cellCurrent = rowSection.getCell(SECTION_R_SECTION_START_TIME_COL);
                String strSectionStartTime = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();

                long sectionTotalTime = -1;
                long sectionStartTime = -1;
                long sectionEndTime = -1;
                try {
//					sectionTotalTimeDate = sdfTime.parse(strSectionTotalTime).getTime();
//					sectionStartTime = sdfTime.parse(strSectionStartTime).getTime();
//					sectionEndTime = sdfTime.parse(strSectionEndTime).getTime();

                    sectionTotalTime = timeToSeconds(strSectionTotalTime);

                } catch (NumberFormatException e) {
                    if(sectionTotalTime == -1) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_TOTAL_TIME_COL).trim() + " הוא לא זמן " + strSectionTotalTime + " ",sectionOutputRow, objFormulaEvaluator);
                    }
                }
                try {
                    sectionStartTime = timeToSeconds(strSectionStartTime);
                } catch (NumberFormatException e) {

                    if(sectionStartTime == -1) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_START_TIME_COL).trim() + " הוא לא זמן " + strSectionStartTime + " ",sectionOutputRow, objFormulaEvaluator);
                    }
                }
                try {
                    sectionEndTime = timeToSeconds(strSectionEndTime);
                } catch (NumberFormatException e) {
                    if(sectionEndTime == -1) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_END_TIME_COL).trim() + " הוא לא זמן " + strSectionEndTime + " ",sectionOutputRow, objFormulaEvaluator);
                    }
                }
//S---test-05---:check if time at the end minus time at the start equals to the time traveled
                if((sectionEndTime >= 0)&&(sectionStartTime >= 0)&&(secondsBetween >= 0)&&(sectionTotalTime >= 0))
                    if((!comparisonDoublesABS(sectionEndTime - sectionStartTime + secondsBetween, sectionTotalTime, maxErrorTime))) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-05: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST05_COL,sectionColNamesList.get(SECTION_R_SECTION_TOTAL_TIME_COL).trim() + " " + strSectionTotalTime +
                                        " לא שווה לחיסור בין " + sectionColNamesList.get(SECTION_R_SECTION_END_TIME_COL).trim() + " " + strSectionEndTime +
                                        " ל- " + sectionColNamesList.get(SECTION_R_SECTION_START_TIME_COL).trim() + " " + strSectionStartTime + " ",sectionOutputRow, objFormulaEvaluator);
                    }


//S---test-09--:check if the average speed is logical
                double speed = 0;
                if(!(nullStrings.contains(strSectionLength)||nullStrings.contains(strSectionTotalTime))) {
                    speed = 60*60*(sectionLength)/(sectionTotalTime);
                    if(!((speed <= maxAvgSpeed)&&(speed >= minAvgSpeed))&&
                            (sectionEndTime - sectionStartTime)!=0){
                        if((sectionTotalTime)!=0) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-09: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST09_COL,"מהירות ממוצעת " + speed + " גדולה מ " +  maxAvgSpeed +" בחיסור הסה\"כ " +
                                            sectionColNamesList.get(SECTION_R_SECTION_LENGTH_COL).trim() + " " + strSectionLength + " " +
                                            sectionColNamesList.get(SECTION_R_SECTION_TOTAL_TIME_COL).trim() + " " + strSectionTotalTime + " ",sectionOutputRow, objFormulaEvaluator);
                        }
                    }
                }
                if(!comparisonDoublesABS(speed, 60*60*(sectionEndLength - sectionStartLength)/(sectionEndTime - sectionStartTime + secondsBetween), maxErrorLength+maxErrorTime))
                    if(!(nullStrings.contains(strSectionEndTime)||nullStrings.contains(strSectionEndTime)||nullStrings.contains(strSectionStartLength)||nullStrings.contains(strSectionEndLength))) {
                        speed = 60*60*(sectionEndLength - sectionStartLength)/(sectionEndTime + secondsBetween - sectionStartTime);
                        if(!(speed <= maxAvgSpeed)&&(speed >= minAvgSpeed)){
                            if((sectionEndTime - sectionStartTime + secondsBetween)!=0) {
                                errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                strSubSectionReport += "Test-09: " +
                                        addToSectionOutput(SECTION_R_OUTPUT_TEST09_COL,"מהירות ממוצעת " + speed + " גדולה מ " +  maxAvgSpeed + " בחיסור ההתחלה וסוף " +
                                                sectionColNamesList.get(SECTION_R_SECTION_END_LENGTH_COL).trim() + " " + strSectionEndLength + " " +
                                                sectionColNamesList.get(SECTION_R_SECTION_START_LENGTH_COL).trim() + strSectionStartLength + " " +
                                                sectionColNamesList.get(SECTION_R_SECTION_END_TIME_COL).trim() + " " + strSectionEndTime + " " +
                                                sectionColNamesList.get(SECTION_R_SECTION_START_TIME_COL).trim() + " " + strSectionStartTime + " ",sectionOutputRow, objFormulaEvaluator);

                            }
                        }
                    }

//S---test-13--:check if the start time is smaller then end time
                if((sectionEndTime>=0)&&(sectionStartTime>=0)&&(secondsBetween>=0))
                    if((sectionEndTime - sectionStartTime + secondsBetween)<0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-13: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST13_COL,sectionColNamesList.get(SECTION_R_SECTION_END_TIME_COL).trim() + " " + strSectionEndTime + " קטן מ" +
                                        sectionColNamesList.get(SECTION_R_SECTION_START_TIME_COL).trim() + " " + strSectionStartTime + " ",sectionOutputRow, objFormulaEvaluator);
                    }

//S---test-13--:check if the start kilometrage is smaller then end kilometrage
                if((sectionEndLength>=0)&&(sectionStartTime>=0))
                    if((sectionEndLength - sectionStartLength)<0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-13: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST13_COL,sectionColNamesList.get(SECTION_R_SECTION_END_LENGTH_COL).trim() + " " + strSectionEndLength + " קטן מ" +
                                        sectionColNamesList.get(SECTION_R_SECTION_START_LENGTH_COL).trim() + " " + strSectionStartLength + " ",sectionOutputRow, objFormulaEvaluator);
                    }




                cellCurrent = rowSection.getCell(SECTION_R_RATE_CODE_COL);
                String strRateCode = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                cellCurrent = rowSection.getCell(SECTION_R_SECTION_CODE_COL);
                String strSectionCode = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                cellCurrent = rowSection.getCell(SECTION_R_SECTION_COST_COL);
                String strSectionCost = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();

//S---test-04---:check if section code matches the rate & cost
                if(!(rates.get(strSectionCode.trim())+"").trim().equals(strRateCode.trim())) {
                    errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                    strSubSectionReport += "Test-04: " +
                            addToSectionOutput(SECTION_R_OUTPUT_TEST04_COL,sectionColNamesList.get(SECTION_R_RATE_CODE_COL).trim() + " " + strRateCode + " לא מתאים ל" + sectionColNamesList.get(SECTION_R_SECTION_CODE_COL).trim()+ " " + strSectionCode + " ",sectionOutputRow, objFormulaEvaluator);
                }

                cellCurrent = rowSection.getCell(SECTION_R_SECTION_PAYMENT_COL);
                String strSectionPayment = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                cellCurrent = rowSection.getCell(SECTION_R_CAR_POLUTION_RATE_COL);
                String strSectionPolutionRate = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();

                double sectionPayment = -1;
                double sectionCost = -1;
                double sectionPolutionRate = -1;


                try {
                    sectionPayment = Double.valueOf(strSectionPayment);
                } catch (NumberFormatException e) {
                    if(sectionPayment == -1) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_PAYMENT_COL).trim() + " הוא לא מספר " + strSectionPayment + " ",sectionOutputRow, objFormulaEvaluator);
                    }
                }
                try {
                    sectionCost = Double.valueOf(strSectionCost);
                    if(sectionCost < 0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() + " הוא שלילי " + sectionCost + " ",sectionOutputRow, objFormulaEvaluator);

                    }
                } catch (NumberFormatException e) {
                    if(sectionCost == -1) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() + " הוא לא מספר " + strSectionCost + " ",sectionOutputRow, objFormulaEvaluator);
                    }
                }
                try {
                    sectionPolutionRate = Double.valueOf(strSectionPolutionRate);
                } catch (NumberFormatException e) {
                    if(sectionCost == -1) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() + " הוא לא מספר " + strSectionPolutionRate + " ",sectionOutputRow, objFormulaEvaluator);
                    }
                }

//S---test-08---:check if price calculated correctly
                if(sectionCost == 0) {
                    if(sectionPayment != 0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-08: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST08_COL,sectionColNamesList.get(SECTION_R_SECTION_PAYMENT_COL).trim() + " " + strSectionPayment +
                                        " לא יכול להיות שונה מ 0 אם  " + sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() +  " " + strSectionCost +  " שווה ל 0 ",sectionOutputRow, objFormulaEvaluator);
                    }
                }
                else if((sectionCost < 0)||(sectionPayment < 0)){
                    if(sectionPayment < 0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-08: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST08_COL,sectionColNamesList.get(SECTION_R_SECTION_PAYMENT_COL).trim() + " " + strSectionPayment + " לא יכול להיות שלילי  ",sectionOutputRow, objFormulaEvaluator);
                    }
                    if(sectionCost < 0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-08: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST08_COL,sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() + " " + strSectionCost + " לא יכול להיות שלילי  ",sectionOutputRow, objFormulaEvaluator);
                    }
                }
                else if(!comparisonDoublesABS((sectionCost+sectionPolutionRate)*sectionLength,sectionPayment,maxErrorPayment)) {
                    errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                    strSubSectionReport += "Test-08: " +
                            addToSectionOutput(SECTION_R_OUTPUT_TEST08_COL,sectionColNamesList.get(SECTION_R_SECTION_PAYMENT_COL).trim() + " " + strSectionPayment +
                                    " לא שווה למכפלה של " + sectionColNamesList.get(SECTION_R_SECTION_LENGTH_COL).trim() + " " + sectionLength +
                                    " ו- " + sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() +  " " + strSectionCost + " ",sectionOutputRow, objFormulaEvaluator);
                }

//S---test-06--:check if there is a section with 0 distance traveled and more then minTime time traveled
                if(sectionTotalTime > timeToSeconds(minTime) + maxErrorTime) {
                    if(sectionLength == 0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-06: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST06_COL, sectionColNamesList.get(SECTION_R_SECTION_TOTAL_TIME_COL).trim() + " גדול מ " + minTime + " " + strSectionTotalTime +
                                        " אבל " + sectionColNamesList.get(SECTION_R_SECTION_LENGTH_COL).trim() + " שווה ל " + strSectionLength + " ",sectionOutputRow, objFormulaEvaluator);
                    }
                }

//S---test-06--:check if there is a section with 0 time traveled and more then minLength distance traveled
                if(sectionLength > minLength + maxErrorLength) {
                    if(sectionTotalTime == 0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-06: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST06_COL,sectionColNamesList.get(SECTION_R_SECTION_LENGTH_COL).trim() + " גדול מ " + minLength + " " + strSectionLength +
                                        " אבל " + sectionColNamesList.get(SECTION_R_SECTION_TOTAL_TIME_COL).trim() + " שווה ל " + strSectionTotalTime + " ",sectionOutputRow, objFormulaEvaluator);   ;
                    }
                }

//S---test-11--:check if there is a time code in section
                if(!(isFreeDay(strStartDate)&&isFreeDay(strEndDate))) {
                    for(int j = 0; j<times.length;j++) {
                        long time = timeToSeconds(times[j]);
//						long time2 = time - timeToSeconds("03:00:00");
                        if((sectionEndTime >= 0)&&(sectionStartTime >= 0)&&(secondsBetween >= 0))
                            if(
                                    ((time>sectionStartTime)&&(time<sectionEndTime+secondsBetween))
//									&&
//									((time2>sectionStartTime)&&(time2<sectionEndTime+secondsBetween))
                            ){
                                if(!(comparisonDoubles(time,sectionStartTime,maxErrorTimeChange)||comparisonDoubles(sectionEndTime,time,maxErrorTimeChange))) {
                                    errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                    strSubSectionReport += "Test-11: " +
                                            addToSectionOutput(SECTION_R_OUTPUT_TEST11_COL, " שעת המעבר " + times[j] + " בין " + sectionColNamesList.get(SECTION_R_SECTION_START_TIME_COL).trim() + " " + strSectionStartTime +
                                                    " ל" + sectionColNamesList.get(SECTION_R_SECTION_END_TIME_COL).trim() + " " + strSectionEndTime + " ב" +
                                                    sectionColNamesList.get(SECTION_R_SECTION_START_DATE_COL).trim() + " " + strStartDate + " ו" +
                                                    sectionColNamesList.get(SECTION_R_SECTION_END_DATE_COL).trim() + " " + strEndDate + " ",sectionOutputRow, objFormulaEvaluator);
                                }
                            }
                    }
                }

                cellCurrent = rowSection.getCell(SECTION_R_TIME_COST_CODE_COL);
                String strTimeCostCode = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                int timeCostCode = -1;
                try {
                    timeCostCode = (int)(double)Double.valueOf(strTimeCostCode);
                    if(timeCostCode<0) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_TIME_COST_CODE_COL).trim() + " שלילי ",sectionOutputRow, objFormulaEvaluator);
                    }
                }catch (NumberFormatException e) {
                    if(nullStrings.contains(strTimeCostCode.trim().toLowerCase())) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_TIME_COST_CODE_COL).trim() + " הוא 'NULL' ",sectionOutputRow, objFormulaEvaluator);
                    }
                    else if(timeCostCode==-1) {
                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                        strSubSectionReport += "Test-01: " +
                                addToSectionOutput(SECTION_R_OUTPUT_TEST01_COL,sectionColNamesList.get(SECTION_R_TIME_COST_CODE_COL).trim() + " הוא לא מספר ",sectionOutputRow, objFormulaEvaluator);
                    }
                }


//				if(strSectionStartTime.contains("23:27:")&&strSectionEndTime.contains("00:02:")) {
//					System.out.println();
//				}


//S---test-12--:check if time code match the time of day
                if(timeCostCode>0) {
                    if(!(isFreeDay(strStartDate)&&isFreeDay(strEndDate))) {
                        for(int j = 0; j<timesStart.length;j++) {
                            long timeStart = timeToSeconds(timesStart[j]);
                            long timeEnd = timeToSeconds(timesEnd[j]);
                            if((sectionStartTime>=timeStart/*-(long)maxErrorTimeChange*/)/*&&(sectionStartTime<=timeEnd)&&
									(sectionEndTime>=timeStart)*/&&(sectionEndTime+secondsBetween<=timeEnd/*+(long)maxErrorTimeChange*/)) {
                                if((timeCostCode != timesCostCode[j])&&(!rates.get(strSectionCode.trim()).equals(0))){
                                    errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                    strSubSectionReport += "Test-12: " +
                                            addToSectionOutput(SECTION_R_OUTPUT_TEST12_COL," שעות המקטע הן " + sectionColNamesList.get(SECTION_R_SECTION_START_TIME_COL).trim() + " " + strSectionStartTime +
                                                    " ו " + sectionColNamesList.get(SECTION_R_SECTION_END_TIME_COL).trim() + " " + strSectionEndTime +
                                                    " הוא בין השעות " + timesStart[j] + " ל " + timesEnd[j] + " ו"+
                                                    sectionColNamesList.get(SECTION_R_TIME_COST_CODE_COL).trim() + " הוא " +
                                                    timeCostCode + " כשהוא צריך להיות " + timesCostCode[j] + " ב " +
                                                    sectionColNamesList.get(SECTION_R_SECTION_START_DATE_COL).trim() + strStartDate +
                                                    sectionColNamesList.get(SECTION_R_SECTION_END_DATE_COL).trim() + strEndDate,sectionOutputRow, objFormulaEvaluator);
                                }

                                if(timesCostCode[j] == costCodeFree) {
                                    if((sectionCost>0)&&(costCodeFree!=sectionCost)) {
                                        errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                        strSubSectionReport += "Test-12: " +
                                                addToSectionOutput(SECTION_R_OUTPUT_TEST12_COL," בשעות פנאי " + sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() + " צריך להיות " +
                                                        costCodeFree + " אבל הוא " + sectionCost + " ",sectionOutputRow, objFormulaEvaluator);
                                    }
                                }
                                else {
                                    HashMap<String, Double> costs = null;
                                    if(timesCostCode[j] == 1) {
                                        costs = costsRush;
                                    }

                                    if(timesCostCode[j] == 2) {
                                        costs = costsLow;
                                    }
                                    if((null != costs)&&(null != strSectionCode)) {
                                        if((sectionCost>0)&&(costs.get(strSectionCode.trim())!=sectionCost)) {
                                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                                            strSubSectionReport += "Test-12: " +
                                                    addToSectionOutput(SECTION_R_OUTPUT_TEST12_COL,sectionColNamesList.get(SECTION_R_RATE_CODE_COL).trim() + " " + strRateCode +
                                                            " לא מתאים ל" + sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim()+ " " + sectionCost + " ",sectionOutputRow, objFormulaEvaluator);
                                        }
                                    }
                                }


                            }
                        }
                    }
                    else {
//S---test-12--:check if time code match the day of the week
                        if(timeCostCode != costCodeFree){
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-12: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST12_COL," בימי חופש " + sectionColNamesList.get(SECTION_R_TIME_COST_CODE_COL).trim() + " צריך להיות " +
                                            costCodeFree + " אבל הוא " + strTimeCostCode,sectionOutputRow, objFormulaEvaluator);  ;
                        }
//S---test-04---:check if section code matches the rate & cost at a free day
                        if((sectionCost>0)&&(costCodeFree!=sectionCost)) {
                            errorsPerDay.put(strStartDateZero, errorsPerDay.get(strStartDateZero)+1);
                            strSubSectionReport += "Test-04: " +
                                    addToSectionOutput(SECTION_R_OUTPUT_TEST04_COL," בימי חופש " + sectionColNamesList.get(SECTION_R_SECTION_COST_COL).trim() + " צריך להיות " +
                                            costCodeFree + " אבל הוא " + sectionCost + " ",sectionOutputRow, objFormulaEvaluator);
                        }
                    }
                }


//S---test-14--:Section code duplication
                if(sectionsCodes.get(rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim())==null) {
                    sectionsCodes.put(rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim(),new LinkedList<Row>());

                }
                else{
                    strSubSectionReport += "Test-14: " +
                            addToSectionOutput(SECTION_R_OUTPUT_TEST14_COL," כבר קיים מקטע עם " + sectionColNamesList.get(SECTION_R_SECTION_ID_COL) + " הזה " + rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim(),sectionOutputRow, objFormulaEvaluator);

                }
                sectionsCodes.get(rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim()).add(rowSection);

//S---test-15--:volume check
                if(peoplePerDay.get(strStartDateZero) == null) {
                    peoplePerDay.put(strStartDateZero, new HashSet<String>());
                }
                peoplePerDay.get(strStartDateZero).add(rowSection.getCell(SECTION_R_CAR_ID_COL).getStringCellValue().trim());

                if(sectionsPerDay.get(strStartDateZero) == null) {
                    sectionsPerDay.put(strStartDateZero, new HashSet<String>());
                }
                sectionsPerDay.get(strStartDateZero).add(rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim());

                if(drivesPerDay.get(strStartDateZero) == null) {
                    drivesPerDay.put(strStartDateZero, new HashSet<String>());
                }
                drivesPerDay.get(strStartDateZero).add(rowSection.getCell(SECTION_R_DRIVE_ID_COL).getStringCellValue().trim());



                if(!strSubSectionReport.equals("")) {
                    Cell cell = sectionOutputRow.createCell(SECTION_R_OUTPUT_CAR_ID_COL);
                    cell.setCellValue(rowSection.getCell(SECTION_R_CAR_ID_COL).getStringCellValue().trim());
                    cell = sectionOutputRow.createCell(SECTION_R_OUTPUT_DRIVE_ID_COL);
                    cell.setCellValue(rowSection.getCell(SECTION_R_DRIVE_ID_COL).getStringCellValue().trim());
                    cell = sectionOutputRow.createCell(SECTION_R_OUTPUT_SECTION_ID_COL);
                    cell.setCellValue(rowSection.getCell(SECTION_R_SECTION_ID_COL).getStringCellValue().trim());
                    cell = sectionOutputRow.createCell(SECTION_R_OUTPUT_ALL_TESTS_COL);
                    cell.setCellValue(right2left_text + strSubSectionReport);
                    SectionReportRowCount++;
                    strSectionReport += strSubSectionReportIndicators + strSubSectionReport + "\n";
                    sectionCountRowsWithErr++;
                }


            }

            int index = entry.getKey().indexOf(KEY_SEPERATOR);
            String sectionKey = entry.getKey();
            String carID = sectionKey.substring(0,index);
            String driveID = sectionKey.substring(index + KEY_SEPERATOR.length());
            String privateKey = carID+KEY_SEPERATOR+driveID;

            String strSubPrivateReportIndicators = carID + "," + driveID + ",";
            String strSubPrivateReport = "";
            Row privateOutputRow = PRIVATE_TRAVEL.createRow(PrivateTravelRowCount);
            Row rowPrivate = null;

            for(int j = PRIVATE_R_OUTPUT_TEST01_COL; j<PRIVATE_R_OUTPUT_ALL_TESTS_COL+1;j++) {
                privateOutputRow.createCell(j);
            }

            if(privateMap.get(privateKey)==null) {
                privateMap.put(privateKey, new LinkedList<>());
                strSubPrivateReport += "Test-00: " + "לא קיימות רשומות בטבלה ";
                privateCountRowsWithErr--;
                privateCountMissingRows++;
            }
            else if(privateMap.get(privateKey).size()>0){
                if(privateMap.get(privateKey).size() != 1) {
                    strSubPrivateReport += "Test-00: " +  "יש יותר מרשומה אחת בטבלה ";
                }

                rowPrivate = privateMap.get(privateKey).get(0);
                privateCountRows++;

//				if(carID.contains("1b0a7741-b5d9-49ed-a697-a060a752eb20")) {
//					System.out.println();
//				}

//P---test-01---:cells values check Null, 0 and 00:00:00.
                for(int j = 0; j<rowPrivate.getLastCellNum();j++) {
                    Cell cell = rowPrivate.getCell(j);
                    String str = getStringFromCol(cell, formulaEvaluatorPrivate);
                    if(formulaEvaluatorPrivate.evaluateInCell(cell).getCellType() == Cell.CELL_TYPE_BLANK) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(j).trim() + " לא יכולה יהיות ריקה ",privateOutputRow, objFormulaEvaluator);
                    }
                    if(null != str) {
                        if(nullStrings.contains(str.toLowerCase().trim())&&!PRIVATE_R_CAN_NULL[j]) {
                            strSubPrivateReport += "Test-01: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(j).trim() + " לא יכול להיות 'NULL'" + str + " ",privateOutputRow, objFormulaEvaluator);

                        }
                        if(zeroStrings.contains(str.toLowerCase())&&!PRIVATE_R_CAN_ZERO[j]) {
                            strSubPrivateReport += "Test-01: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(j).trim() + " לא יכול להיות '0'" + str + " ",privateOutputRow, objFormulaEvaluator);
                        }
                        if(zeroTimeStrings.contains(str.toLowerCase().trim())&&!PRIVATE_R_CAN_ZERO_TIME[j]) {
                            strSubPrivateReport += "Test-01: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(j).trim() + " לא יכול להיות '00:00:00'" + str + " ",privateOutputRow, objFormulaEvaluator);
                        }
                    }
                }

                Cell cellCurrent = rowPrivate.getCell(PRIVATE_R_DRIVE_ID_COL);
                String strDriveCode = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                List<Row> sections = new LinkedList<>();
                HashMap<String,List<Row>> firsts = new HashMap<String,List<Row>>();

                long privateTotalTime = 0;
                long privateTotalPayment = 0;
                for(int i = 0; i<entry.getValue().size();i++) {
                    Row rowSection = entry.getValue().get(i);

//P---test-02-------:calculate sum of sections from section report
                    cellCurrent = rowSection.getCell(SECTION_R_SECTION_TOTAL_TIME_COL);
                    objFormulaEvaluator.evaluate(cellCurrent);
                    String strSectionTotalTime = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
                    long sectionTotalTime = -1;
                    try {
                        sectionTotalTime = timeToSeconds(strSectionTotalTime);
                        privateTotalTime += sectionTotalTime;
                    } catch (NumberFormatException e) {
//						System.out.println(strSectionTotalTime);
                    }

//P---test-03-------:create sum of section payments from section report
                    cellCurrent = rowSection.getCell(SECTION_R_SECTION_PAYMENT_COL);
                    String strSectionPayment = getStringFromCol(cellCurrent, formulaEvaluatorSection).trim();
                    double sectionPayment = -1;
                    try {
                        sectionPayment = Double.valueOf(strSectionPayment);
                        privateTotalPayment += sectionPayment;
                    } catch (NumberFormatException e) {

                    }

                    cellCurrent = rowSection.getCell(SECTION_R_DRIVE_ID_COL);
                    String strDriveCode2 = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    if(strDriveCode2.equals(strDriveCode)) {
                        sections.add(rowSection);
                    }
                }



//P---test-02---:compare sum of sections to sum in private report
                cellCurrent = rowPrivate.getCell(PRIVATE_R_DRIVE_TOTAL_TIME_COL);
                objFormulaEvaluator.evaluate(cellCurrent);
                String strPrivateTotalTime = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();

                long privateTotalTime2 = -1;
                try {
                    privateTotalTime2 = timeToSeconds(strPrivateTotalTime);
                    if(!comparisonDoublesABS(privateTotalTime2, privateTotalTime, maxErrorTime)) {

                        if(carID.equals("91175998-647b-4123-bf94-695efa2a885f")){
                            privateTotalTime2 = timeToSeconds(strPrivateTotalTime);
                        }
                        strSubPrivateReport += "Test-02: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST02_COL,privateColNamesList.get(PRIVATE_R_DRIVE_TOTAL_TIME_COL).trim() + " " + secondsToTime(privateTotalTime2) +
                                        " לא שווה לסכום של " + sectionColNamesList.get(SECTION_R_SECTION_TOTAL_TIME_COL).trim() + " " + secondsToTime(privateTotalTime) + " ",privateOutputRow, objFormulaEvaluator);
                    }
                } catch (NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_TOTAL_TIME_COL).trim() + " הוא לא זמן " + strPrivateTotalTime + " ",privateOutputRow, objFormulaEvaluator);
                }

//P---test-03---:compare sum of section payments to sum in private report
                double privateTotalPayment2 = -1;
                cellCurrent = rowPrivate.getCell(PRIVATE_R_ACTUAL_PAYMENT_COL);
                String strPrivatePayment = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                try {
                    privateTotalPayment2 = Double.parseDouble(strPrivatePayment);
                    if(!comparisonDoublesABS(privateTotalPayment2, privateTotalPayment, maxErrorPayment)) {
                        if(!((privateTotalPayment>privateTotalPayment2)&&(privateTotalPayment2==maxPayment))) {
                            strSubPrivateReport += "Test-03: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST03_COL,privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL).trim() + " " + privateTotalPayment2 + " לא שווה לסכום של " +
                                            sectionColNamesList.get(SECTION_R_SECTION_PAYMENT_COL).trim() + " " + privateTotalPayment + " ",privateOutputRow, objFormulaEvaluator);
                            ;
                        }
                    }

//P---test-04---:check if the payment is less then maxPayment
                    if((privateTotalPayment2>maxPayment)&&(!comparisonDoubles(privateTotalPayment2, maxPayment, maxErrorPayment))) {
                        strSubPrivateReport += "Test-04: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST04_COL,privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL).trim() + " " + strPrivatePayment + " גדול מ" + maxPayment,privateOutputRow, objFormulaEvaluator);
                    }
                } catch (NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL).trim() + " הוא לא מספר " + strPrivatePayment + " ",privateOutputRow, objFormulaEvaluator);
                }

//P---test-05---:check if the average speed is logical
                cellCurrent = rowPrivate.getCell(PRIVATE_R_DRIVE_LENGTH_COL);
                String strDriveLength = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                double driveLength = -1;
                try {
                    driveLength = Double.valueOf(strDriveLength);
                    if(driveLength<0) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_LENGTH_COL).trim() + " " + driveLength + " שלילי " + " ",privateOutputRow, objFormulaEvaluator);
                    }
                    else {
                        if(privateTotalTime2>0) {
                            double speed = 60.0*60.0*driveLength/privateTotalTime2;
                            if(speed > maxAvgSpeed||speed < minAvgSpeed) {
                                strSubPrivateReport += "Test-05: " +
                                        addToPrivateOutput(PRIVATE_R_OUTPUT_TEST05_COL,"מהירות ממוצעת " + speed + " גדולה מ " +  maxAvgSpeed + " " +
                                                privateColNamesList.get(PRIVATE_R_DRIVE_LENGTH_COL).trim() + " " + strDriveLength + " " +
                                                privateColNamesList.get(PRIVATE_R_DRIVE_TOTAL_TIME_COL).trim() + " " + strPrivateTotalTime + " ",privateOutputRow, objFormulaEvaluator);
                            }
                        }

//P---test-11--:check if there is a drive with 0 distance traveled and more then minTime time traveled
                        if(privateTotalTime2 > timeToSeconds(minTime) + maxErrorTime) {
                            if(driveLength == 0) {
                                strSubPrivateReport += "Test-11: " +
                                        addToPrivateOutput(PRIVATE_R_OUTPUT_TEST11_COL,privateColNamesList.get(PRIVATE_R_DRIVE_TOTAL_TIME_COL).trim() + " גדול מ " + minTime + " " + secondsToTime(privateTotalTime2) +
                                                " אבל " + privateColNamesList.get(PRIVATE_R_DRIVE_LENGTH_COL).trim() + " שווה ל " + strDriveLength + " ",privateOutputRow, objFormulaEvaluator);
                            }
                        }

//P---test-11--:check if there is a drive with 0 time traveled and more then minLength distance traveled
                        if(driveLength > minLength + maxErrorLength) {
                            if(privateTotalTime2 == 0) {
                                strSubPrivateReport += "Test-11: " +
                                        addToPrivateOutput(PRIVATE_R_OUTPUT_TEST11_COL,privateColNamesList.get(PRIVATE_R_DRIVE_LENGTH_COL).trim() + " גדול מ " + minLength + " " + strDriveLength +
                                                " אבל " + privateColNamesList.get(PRIVATE_R_DRIVE_TOTAL_TIME_COL).trim() + " שווה ל " + secondsToTime(privateTotalTime2) + " ",privateOutputRow, objFormulaEvaluator);
                            }
                        }
                    }



                } catch (NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_TOTAL_TIME_COL).trim() + " הוא לא זמן " + strPrivateTotalTime + " ",privateOutputRow, objFormulaEvaluator);
                }


                cellCurrent = rowPrivate.getCell(PRIVATE_R_FIRST_FOUR_COL);
                String strPrivateFirstFour = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_START_DATE_COL);
                objFormulaEvaluator.evaluate(cellCurrent);
                String strPrivateStartDate = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_END_DATE_COL);
                objFormulaEvaluator.evaluate(cellCurrent);
//				String strPrivateEndDate = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
				cellCurrent = rowPrivate.getCell(PRIVATE_R_START_TIME_COL);
				objFormulaEvaluator.evaluate(cellCurrent);
				String strPrivateStartTime = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
//				cellCurrent = rowPrivate.getCell(PRIVATE_R_END_TIME_COL);
//				objFormulaEvaluator.evaluate(cellCurrent);
//				String strPrivateEndtime = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();

//				if(privateKey.contains("97560eb0-d550-4419-8e5b-7cf793dd5aea")) {
//					System.out.println();
//				}


//				if(privateKey.contains("17a017ba-1772-4c27-b8fb-15ec0d71d60f")) {
//					System.out.println();
//				}

                cellCurrent = rowPrivate.getCell(PRIVATE_R_DRIVE_START_LONGITUDE_COL);
                String strDriveStartLongitude = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_DRIVE_START_LATITUDE_COL);
                String strDriveStartLatitude = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_DRIVE_END_LONGITUDE_COL);
                String strDriveEndLongitude = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_DRIVE_END_LATITUDE_COL);
                String strDriveEndLatitude = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_PREVIOUS_DRIVE_CODE_COL);
                String strPreviousDriveCode = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();



                double driveStartLongitude = -1;
                double driveStartLatitude = -1;
                double driveEndLongitude = -1;
                double driveEndLatitude = -1;


                try {
                    driveStartLatitude = Double.valueOf(strDriveStartLatitude);
                    if(driveStartLatitude<0) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_START_LATITUDE_COL) + " שלילי " + strDriveStartLatitude + " ",privateOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_START_LATITUDE_COL) + " הוא לא מספר " + strDriveStartLatitude + " ",privateOutputRow, objFormulaEvaluator);
                }
                try {
                    driveStartLongitude = Double.parseDouble(strDriveStartLongitude);
                    if(driveStartLongitude<0) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_START_LONGITUDE_COL) + " שלילי " + strDriveStartLongitude + " ",privateOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_START_LONGITUDE_COL) + " הוא לא מספר " + strDriveStartLongitude + " ",privateOutputRow, objFormulaEvaluator);
                }
                try {
                    driveEndLatitude = Double.parseDouble(strDriveEndLatitude);
                    if(driveEndLatitude<0) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_END_LATITUDE_COL) + " שלילי " + strDriveEndLatitude + " ",privateOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_END_LATITUDE_COL) + " הוא לא מספר " + strDriveEndLatitude + " ",privateOutputRow, objFormulaEvaluator);
                }
                try {
                    driveEndLongitude = Double.parseDouble(strDriveEndLongitude);
                    if(driveEndLongitude<0) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_END_LONGITUDE_COL) + " שלילי " + strDriveEndLongitude + " ",privateOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_DRIVE_END_LONGITUDE_COL) + " הוא לא מספר " + strDriveEndLongitude + " ",privateOutputRow, objFormulaEvaluator);
                }



//P---test-06---:Previous drive exists
                Row rowPrivatePre = null;


                if(!nullStrings.contains(strPreviousDriveCode)) {
                    for(int j = privateRowList.size()/2, jump = privateRowList.size()/2;
                        (j<privateRowList.size())&&(j>=0)&&(jump!=0)&&(null == rowPrivatePre)&&(!nullStrings.contains(strPreviousDriveCode));
                        j+=jump) {
                        rowPrivatePre = privateRowList.get(j);
                        Cell cellDrive = rowPrivatePre.getCell(PRIVATE_R_DRIVE_ID_COL);
                        objFormulaEvaluator.evaluate(cellDrive);
                        String strDrive = objDefaultFormat.formatCellValue(cellDrive,objFormulaEvaluator).trim();
                        int jumpDir = strDrive.trim().compareToIgnoreCase(strPreviousDriveCode.trim());
                        if(jumpDir > 0) {
                            rowPrivatePre = null;
                            //						int remainder = Math.abs(jump)%2;
                            jump = (-1)*Math.abs(jump)/2;
                            //						if(jump != 0)
                            //							  jump -= remainder;
                        }
                        else if(jumpDir < 0) {
                            rowPrivatePre = null;
                            //						int remainder = Math.abs(jump)%2;
                            jump = Math.abs(jump)/2 + jump%2;
                            //						if(jump != 0)
                            //							  jump += remainder;
                        }
                        else{
                            jump = 0;
                        }

                        if(Math.abs(jump)<10) {
//							if(strPreviousDriveCode.contains("929c293a-3407-407a-9c3d-5a302a34c6a2")) {
//								int y = 10;
//							}
                            int startJ = j;
                            jump = Math.abs(jump)*2;
                            j = startJ-2*jump;
                            for(j = startJ-2*jump;(j<privateRowList.size())&&(j<startJ+2*jump);j++) {
                                if(j<0) {
                                    j = 0;
                                }
                                rowPrivatePre = privateRowList.get(j);
                                cellDrive = rowPrivatePre.getCell(PRIVATE_R_DRIVE_ID_COL);
                                objFormulaEvaluator.evaluate(cellDrive);
                                strDrive = objDefaultFormat.formatCellValue(cellDrive,objFormulaEvaluator).trim();
                                if(strDrive.trim().equalsIgnoreCase(strPreviousDriveCode.trim())) {
                                    j = privateRowList.size();
                                }
                                else {
                                    rowPrivatePre = null;
                                }
                            }
                        }
                    }
                }
                if((null == rowPrivatePre)&&(!nullStrings.contains(strPreviousDriveCode.trim()))) {
                    strSubPrivateReport += "Test-06: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST06_COL," יש " + privateColNamesList.get(PRIVATE_R_PREVIOUS_DRIVE_CODE_COL).trim() + " " + strPreviousDriveCode +
                                    " אבל אין " + privateColNamesList.get(PRIVATE_R_DRIVE_ID_COL).trim() + " בשורה אחרת של קוד הרכב הזה ",privateOutputRow, objFormulaEvaluator);
                    ;
                }

//P---test-07---:Previous drive the values match
                else if (null != rowPrivatePre){
                    cellCurrent = rowPrivate.getCell(PRIVATE_R_PREVIOUS_DRIVE_END_LONGITUDE_COL);
                    String strPreDriveEndLongitude = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    cellCurrent = rowPrivate.getCell(PRIVATE_R_PREVIOUS_DRIVE_END_LATITUDE_COL);
                    String strPreDriveEndLatitude = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();

                    double preDriveEndLongitude = -1;
                    double preDriveEndLatitude = -1;
                    try {
                        preDriveEndLatitude = Double.parseDouble(strPreDriveEndLatitude);
                        if(preDriveEndLatitude<0) {
                            strSubPrivateReport += "Test-01: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_PREVIOUS_DRIVE_END_LATITUDE_COL) + " שלילי " + strPreDriveEndLatitude + " וקיימת נסיעה קודמת ",privateOutputRow, objFormulaEvaluator);
                        }
                    }catch(NumberFormatException e) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_PREVIOUS_DRIVE_END_LATITUDE_COL) + " הוא לא מספר " + strPreDriveEndLatitude + " וקיימת נסיעה קודמת ",privateOutputRow, objFormulaEvaluator);
                    }
                    try {
                        preDriveEndLongitude = Double.parseDouble(strPreDriveEndLongitude);
                        if(preDriveEndLongitude<0) {
                            strSubPrivateReport += "Test-01: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_PREVIOUS_DRIVE_END_LONGITUDE_COL) + " שלילי " + strPreDriveEndLongitude + " וקיימת נסיעה קודמת ",privateOutputRow, objFormulaEvaluator);
                        }
                    }catch(NumberFormatException e) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_PREVIOUS_DRIVE_END_LONGITUDE_COL) + " הוא לא מספר " + strPreDriveEndLongitude + " וקיימת נסיעה קודמת ",privateOutputRow, objFormulaEvaluator);
                    }

                    cellCurrent = rowPrivatePre.getCell(PRIVATE_R_START_DATE_COL);
                    objFormulaEvaluator.evaluate(cellCurrent);
                    String strPrivatePreStartDate = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
                    cellCurrent = rowPrivatePre.getCell(PRIVATE_R_END_DATE_COL);
                    objFormulaEvaluator.evaluate(cellCurrent);
                    String strPrivatePreEndDate = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
//					cellCurrent = rowPrivatePre.getCell(PRIVATE_R_START_TIME_COL);
//					objFormulaEvaluator.evaluate(cellCurrent);
//					String strPrivatePreStartTime = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator);
                    cellCurrent = rowPrivatePre.getCell(PRIVATE_R_END_TIME_COL);
                    objFormulaEvaluator.evaluate(cellCurrent);
                    String strPrivatePreEndTime = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();

                    cellCurrent = rowPrivate.getCell(PRIVATE_R_PREVIOUS_END_TIME_COL);
                    objFormulaEvaluator.evaluate(cellCurrent);
                    String strPrivatePreEndTime2 = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
                    cellCurrent = rowPrivate.getCell(PRIVATE_R_PREVIOUS_END_DATE_COL);
                    objFormulaEvaluator.evaluate(cellCurrent);
                    String strPrivatePreEndDate2 = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();

                    cellCurrent = rowPrivatePre.getCell(PRIVATE_R_DRIVE_END_LONGITUDE_COL);
                    String strPreDriveEndLongitude2 = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    cellCurrent = rowPrivatePre.getCell(PRIVATE_R_DRIVE_END_LATITUDE_COL);
                    String strPreDriveEndLatitude2 = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();


                    double preDriveEndLongitude2 = -1;
                    double preDriveEndLatitude2 = -1;

                    try {
                        preDriveEndLongitude2 = Double.parseDouble(strPreDriveEndLongitude2);
                        preDriveEndLatitude2 = Double.parseDouble(strPreDriveEndLatitude2);
                    }catch(NumberFormatException e) {

                    }

                    if(!(strPrivatePreEndDate2.equals(strPrivatePreEndDate))) {
                        strSubPrivateReport += "Test-07: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST07_COL,privateColNamesList.get(PRIVATE_R_PREVIOUS_END_DATE_COL).trim() + " " + strPrivatePreEndDate2 + "-" +
                                        privateColNamesList.get(PRIVATE_R_END_DATE_COL).trim() + " " + strPrivatePreEndDate +" לא שווים ",privateOutputRow, objFormulaEvaluator); ;
                    }

                    if(!(strPrivatePreEndTime2.equals(strPrivatePreEndTime))) {
                        strSubPrivateReport += "Test-07: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST07_COL,privateColNamesList.get(PRIVATE_R_PREVIOUS_END_TIME_COL).trim() + " " + strPrivatePreEndTime2 + "-" +
                                        privateColNamesList.get(PRIVATE_R_END_TIME_COL).trim() + " " + strPrivatePreEndTime +" לא שווים ",privateOutputRow, objFormulaEvaluator);
                    }
                    if(!(comparisonDoublesABS(preDriveEndLongitude2, preDriveEndLongitude, maxCoordinatesError)&&
                            comparisonDoublesABS(preDriveEndLatitude2, preDriveEndLatitude, maxCoordinatesError))) {
                        strSubPrivateReport += "Test-07: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST07_COL," לסיום המקטע הקודם יש נ.צ. " + strPreDriveEndLatitude2 + " " + strPreDriveEndLongitude2 + " והם שונים מהנ.צ. שרשומים במקטע זה " +
                                        strPreDriveEndLatitude + " " + strPreDriveEndLongitude + " ",privateOutputRow, objFormulaEvaluator);;

                    }



//					if(privateKey.contains("b9ec909f-4a5b-4a94-89e0-86008f870477")) {
//						System.out.println();
//					}

                    SimpleDateFormat sdfTime = new SimpleDateFormat("dd/MM/yyyy");
                    double secondsBetween = -1;
                    try {
                        secondsBetween = (sdfTime.parse(strPrivatePreEndDate2).getTime()-sdfTime.parse(strPrivateStartDate).getTime())/1000;
                    } catch (ParseException e1) {

                    }

//P---test-14-------:check if the previous drive ended more than minSecondsBetweenDrives ago
                    if(secondsBetween >= 0) {
                        if (timeToSeconds(strPrivateStartTime) + secondsBetween >= timeToSeconds(strPrivatePreEndTime2)) {
                            if (comparisonDoubles(timeToSeconds(strPrivateStartTime) + secondsBetween, timeToSeconds(strPrivatePreEndTime2), minSecondsBetweenDrives+maxErrorTime)) {
                                strSubPrivateReport += "Test-14: " +
                                        addToPrivateOutput(PRIVATE_R_OUTPUT_TEST14_COL, privateColNamesList.get(PRIVATE_R_START_TIME_COL).trim() + " הוא " + strPrivateStartTime +
                                                " אבל " + privateColNamesList.get(PRIVATE_R_END_TIME_COL).trim() + " של הנסיעה הקודמת הוא " + strPrivatePreEndTime + " והחיסור ביניהם " +
                                                " קטן מ " + minTimeBetweenDrives, privateOutputRow, objFormulaEvaluator);
                            }
                        }

//P---test-15-------:check if the previous drive ended before this drive started
                        else {
                            strSubPrivateReport += "Test-15: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST15_COL, privateColNamesList.get(PRIVATE_R_START_TIME_COL).trim() + strPrivateStartTime +
                                            " קטן מ" + privateColNamesList.get(PRIVATE_R_START_TIME_COL).trim() + " של הנסיעה הקודמת " + strPrivatePreEndTime, privateOutputRow, objFormulaEvaluator);
                        }
                    }


//P---test-08-------:check if it is the first drive of the day
                    Cell cellDrive = rowPrivatePre.getCell(PRIVATE_R_DRIVE_ID_COL);
                    objFormulaEvaluator.evaluate(cellDrive);
                    String strDrive = objDefaultFormat.formatCellValue(cellDrive,objFormulaEvaluator).trim();
                    if(strPrivateFirstFour.equalsIgnoreCase("yes")) {
                        if(strPrivateStartDate.equals(strPrivatePreStartDate)) {
                            strSubPrivateReport += "Test-08: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST08_COL," ב" + privateColNamesList.get(PRIVATE_R_FIRST_FOUR_COL).trim() + " יש " + strPrivateFirstFour +
                                            " אבל " + privateColNamesList.get(PRIVATE_R_START_DATE_COL).trim() + " " + strPrivateStartDate + " " +
                                            " שווה ל" + privateColNamesList.get(PRIVATE_R_START_DATE_COL).trim() + " של הנסיעה הקודמת " + strPrivatePreStartDate + " " + strDrive + " ",privateOutputRow, objFormulaEvaluator); ;
                        }
                        if(driveLength > firstKMs + maxErrorLength) {
                            strSubPrivateReport += "Test-08: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST08_COL," ב" + privateColNamesList.get(PRIVATE_R_FIRST_FOUR_COL).trim() + " יש " + strPrivateFirstFour +
                                            " אבל " + privateColNamesList.get(PRIVATE_R_DRIVE_LENGTH_COL).trim() + " " + strDriveLength + " " +
                                            " גדול מ " + firstKMs + " ",privateOutputRow, objFormulaEvaluator);
                        }
                    }
                    else {
                        if(!strPrivateStartDate.equals(strPrivatePreStartDate)) {
                            strSubPrivateReport += "Test-08: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST08_COL," ב" + privateColNamesList.get(PRIVATE_R_FIRST_FOUR_COL).trim() + " יש " + strPrivateFirstFour +
                                            " אבל " + privateColNamesList.get(PRIVATE_R_START_DATE_COL).trim() + " " + strPrivateStartDate + " " +
                                            " שונה מ" + privateColNamesList.get(PRIVATE_R_START_DATE_COL).trim() + " של הנסיעה הקודמת " + strPrivatePreStartDate + " ",privateOutputRow, objFormulaEvaluator);
                            ;
                        }
                    }
                }

//P---test-08---:check if it is the only first drive of the day
                if(strPrivateFirstFour.equalsIgnoreCase("yes")) {
                    if(firsts.get(carID + KEY_SEPERATOR + strPrivateStartDate)==null){
                        firsts.put(carID + KEY_SEPERATOR + strPrivateStartDate,new LinkedList<Row>());
                    }
                    else {
                        Row row = firsts.get(carID + KEY_SEPERATOR + strPrivateStartDate).get(0);
                        Cell cell = row.getCell(PRIVATE_R_DRIVE_ID_COL);
                        objFormulaEvaluator.evaluate(cell);
                        String otherDriveID = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
                        cell = row.getCell(PRIVATE_R_FIRST_FOUR_COL);
                        objFormulaEvaluator.evaluate(cell);
                        String firstFour = objDefaultFormat.formatCellValue(cellCurrent,objFormulaEvaluator).trim();
                        strSubPrivateReport += "Test-08: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST08_COL," ב" + privateColNamesList.get(PRIVATE_R_FIRST_FOUR_COL).trim() +
                                        " יש " + strPrivateFirstFour + " בתאריך " + strPrivateStartDate +
                                        " אבל כבר קיימת נסיעה נוספת באותו תאריך " + otherDriveID + " שמסומנת " + firstFour,privateOutputRow, objFormulaEvaluator);

                    }
                    firsts.get(carID + KEY_SEPERATOR + strPrivateStartDate).add(rowPrivate);
                }

//P---test-13--:If first drive of the day and length under 4 marked yes
                else if(driveLength + maxErrorLength < firstKMs) {
                    boolean first = false;
                    if (rowPrivatePre != null) {
                        cellCurrent = rowPrivatePre.getCell(PRIVATE_R_START_DATE_COL);
                        objFormulaEvaluator.evaluate(cellCurrent);
                        String strPrivatePreStartDate = objDefaultFormat.formatCellValue(cellCurrent, objFormulaEvaluator).trim();
                        if (!strPrivateStartDate.equals(strPrivatePreStartDate)) {
                            first = true;
                        }
                    }
                    else{
                        first = true;
                    }
                    if (first&&(driveLength>0)) {
                        strSubPrivateReport += "Test-13: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST13_COL, " ב" + privateColNamesList.get(PRIVATE_R_FIRST_FOUR_COL).trim() +
                                        " יש " + strPrivateFirstFour + " בתאריך " + strPrivateStartDate +
                                        " אבל לא קיימת נסיעה קודמת באותו תאריך ומרחק הנסיעה " + driveLength + " קטן מ " + firstKMs, privateOutputRow, objFormulaEvaluator);
                    }
                }




//P---test-09---:Start and end sections have the same coordinates as the drive
                Row rowDriveStartSection = null, rowDriveEndSection = null;
                for(int j = 0; (j < sections.size())&&((rowDriveStartSection == null)||( rowDriveEndSection == null));j++) {
                    Row rowPre = sections.get(j);
                    cellCurrent = rowPre.getCell(SECTION_R_SECTION_START_LONGITUDE_COL);
                    String strPreDriveStartLongitude3 = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    cellCurrent = rowPre.getCell(SECTION_R_SECTION_START_LATITUDE_COL);
                    String strPreDriveStartLatitude3 = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    cellCurrent = rowPre.getCell(SECTION_R_SECTION_END_LONGITUDE_COL);
                    String strPreDriveEndLongitude3= getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    cellCurrent = rowPre.getCell(SECTION_R_SECTION_END_LATITUDE_COL);
                    String strPreDriveEndLatitude3 = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    cellCurrent = rowPre.getCell(SECTION_R_SECTION_ID_COL);
                    String strPreSectionID = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                    if(sections.size()>1) {
                        if(strPreSectionID.endsWith("-1")) {
                            double preDriveStartLongitude = -1;
                            double preDriveStartLatitude = -1;
                            try {
                                preDriveStartLongitude = Double.valueOf(strPreDriveStartLongitude3);
                                preDriveStartLatitude = Double.valueOf(strPreDriveStartLatitude3);
                                if((comparisonDoublesABS(preDriveStartLongitude, driveStartLongitude, maxCoordinatesError))&&(comparisonDoublesABS(preDriveStartLatitude, driveStartLatitude, maxCoordinatesError))) {
                                    rowDriveStartSection = rowPre;
                                }
                            }catch(NumberFormatException e) {

                            }
                        }
                        else {
                            double preDriveEndLongitude2 = -1;
                            double preDriveEndLatitude2 = -1;
                            try {
                                preDriveEndLongitude2 = Double.valueOf(strPreDriveEndLongitude3);
                                preDriveEndLatitude2 = Double.valueOf(strPreDriveEndLatitude3);
                                if((comparisonDoublesABS(preDriveEndLongitude2, driveEndLongitude, maxCoordinatesError))&&(comparisonDoublesABS(preDriveEndLatitude2, driveEndLatitude, maxCoordinatesError))) {
                                    rowDriveEndSection = rowPre;
                                }
                            }catch(NumberFormatException e) {

                            }
                        }
                    }
                    else {
                        double preDriveStartLongitude = -1;
                        double preDriveStartLatitude = -1;
                        double preDriveEndLongitude2 = -1;
                        double preDriveEndLatitude2 = -1;
                        try {
                            preDriveStartLongitude = Double.valueOf(strPreDriveStartLongitude3);
                            preDriveStartLatitude = Double.valueOf(strPreDriveStartLatitude3);
                            preDriveEndLongitude2 = Double.valueOf(strPreDriveEndLongitude3);
                            preDriveEndLatitude2 = Double.valueOf(strPreDriveEndLatitude3);
                            if((comparisonDoublesABS(preDriveEndLongitude2, driveEndLongitude, maxCoordinatesError))&&(comparisonDoublesABS(preDriveEndLatitude2, driveEndLatitude, maxCoordinatesError))) {
                                rowDriveEndSection = rowPre;
                            }
                            if((comparisonDoublesABS(preDriveStartLongitude, driveStartLongitude, maxCoordinatesError))&&(comparisonDoublesABS(preDriveStartLatitude, driveStartLatitude, maxCoordinatesError))) {
                                rowDriveStartSection = rowPre;
                            }
                        }catch(NumberFormatException e) {

                        }
                    }
                }

                if((rowDriveStartSection == null)||( rowDriveEndSection == null)) {
                    strSubPrivateReport += "Test-09: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST09_COL," לא נמצאו מקטעים עם נ.צ. מתאימים לנסיעה זו",privateOutputRow, objFormulaEvaluator);
                }

//P---test-10---:Actual payment calculation check
                cellCurrent = rowPrivate.getCell(PRIVATE_R_INITIAL_COST_COL);
                String strInitialCost = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_POLLUTION_DIF_COL);
                String strPollutionDif = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_POLLUTION_TOTAL_COST_COL);
                String strPollutionTotalCost = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();
                cellCurrent = rowPrivate.getCell(PRIVATE_R_ACTUAL_PAYMENT_COL);
                String strActualPayment = getStringFromCol(cellCurrent, formulaEvaluatorPrivate).trim();


                double initialCost = -1;
                double pollutionDif = -100;
                double pollutionTotalCost = -100;
                double actualPayment = -1;

                try {
                    initialCost = Double.valueOf(strInitialCost);
                    if(initialCost<0) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_INITIAL_COST_COL) + " שלילי " + strDriveStartLatitude + " ",privateOutputRow, objFormulaEvaluator);

                    }
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_INITIAL_COST_COL) + " הוא לא מספר " + strDriveStartLatitude + " ",privateOutputRow, objFormulaEvaluator);
                }
                try {
                    actualPayment = Double.valueOf(strActualPayment);
                    if(actualPayment<0) {
                        strSubPrivateReport += "Test-01: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL) + " שלילי " + strActualPayment + " ",privateOutputRow, objFormulaEvaluator);
                    }
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL) + " הוא לא מספר " + strActualPayment + " ",privateOutputRow, objFormulaEvaluator);
                }
                try {
                    pollutionDif = Double.valueOf(strPollutionDif);
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_POLLUTION_DIF_COL) + " הוא לא מספר " + strPollutionDif + " ",privateOutputRow, objFormulaEvaluator);
                }
                try {
                    pollutionTotalCost = Double.valueOf(strPollutionTotalCost);
                }catch(NumberFormatException e) {
                    strSubPrivateReport += "Test-01: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST01_COL,privateColNamesList.get(PRIVATE_R_POLLUTION_TOTAL_COST_COL) + " הוא לא מספר " + strPollutionTotalCost + " ",privateOutputRow, objFormulaEvaluator);
                }

                if(initialCost>0) {
                    if((pollutionDif!=-100)&&(pollutionTotalCost!=-100)) {
                        if(!comparisonDoublesABS(driveLength*pollutionDif, pollutionTotalCost, maxErrorPayment)) {
                            strSubPrivateReport += "Test-10: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST10_COL,privateColNamesList.get(PRIVATE_R_POLLUTION_TOTAL_COST_COL) + " " +pollutionTotalCost +" לא שווה למכפלה של " +
                                            privateColNamesList.get(PRIVATE_R_POLLUTION_DIF_COL) + " " + pollutionDif + " ב" +
                                            privateColNamesList.get(PRIVATE_R_DRIVE_LENGTH_COL) + " " + driveLength + " ",privateOutputRow, objFormulaEvaluator);
                        }
                        else if(!comparisonDoublesABS(pollutionTotalCost+initialCost, actualPayment, maxErrorPayment)){
                            strSubPrivateReport += "Test-10: " +
                                    addToPrivateOutput(PRIVATE_R_OUTPUT_TEST10_COL,privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL) + actualPayment +" לא שווה לחיבור של " +
                                            privateColNamesList.get(PRIVATE_R_INITIAL_COST_COL) + " " + initialCost + " ב" +
                                            privateColNamesList.get(PRIVATE_R_POLLUTION_TOTAL_COST_COL) + " " + pollutionTotalCost + " ",privateOutputRow, objFormulaEvaluator);
                        }
                    }
                }
                else if(initialCost == 0){
                    if(actualPayment!=0) {
                        strSubPrivateReport += "Test-10: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST10_COL,privateColNamesList.get(PRIVATE_R_INITIAL_COST_COL) + initialCost +" שווה ל 0 אבל " +
                                        privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL) + " " + actualPayment + " שונה מ 0 ",privateOutputRow, objFormulaEvaluator);
                    }
                    if(pollutionTotalCost!=0) {
                        strSubPrivateReport += "Test-10: " +
                                addToPrivateOutput(PRIVATE_R_OUTPUT_TEST10_COL,privateColNamesList.get(PRIVATE_R_POLLUTION_TOTAL_COST_COL) + pollutionTotalCost +" שווה ל 0 אבל " +
                                        privateColNamesList.get(PRIVATE_R_ACTUAL_PAYMENT_COL) + " " + actualPayment + " שונה מ 0 ",privateOutputRow, objFormulaEvaluator);
                    }
                }

//P---test-12--:Private code duplication
                if(drivesCodes.get(rowPrivate.getCell(PRIVATE_R_DRIVE_ID_COL).getStringCellValue().trim())==null) {
                    drivesCodes.put(rowPrivate.getCell(PRIVATE_R_DRIVE_ID_COL).getStringCellValue().trim(),new LinkedList<Row>());
                }
                else {
                    strSubPrivateReport += "Test-12: " +
                            addToPrivateOutput(PRIVATE_R_OUTPUT_TEST12_COL," כבר קיימת נסיעה עם " + privateColNamesList.get(PRIVATE_R_DRIVE_ID_COL) + " הזה " + rowPrivate.getCell(PRIVATE_R_DRIVE_ID_COL).getStringCellValue().trim(),privateOutputRow, objFormulaEvaluator);

                }
                drivesCodes.get(rowPrivate.getCell(PRIVATE_R_DRIVE_ID_COL).getStringCellValue().trim());





            }
            if(!strSubPrivateReport.equals("")) {

                Cell cell = privateOutputRow.createCell(PRIVATE_R_OUTPUT_CAR_ID_COL);
                cell.setCellValue(carID);
                cell = privateOutputRow.createCell(PRIVATE_R_OUTPUT_DRIVE_ID_COL);
                cell.setCellValue(driveID);
                cell = privateOutputRow.createCell(PRIVATE_R_OUTPUT_ALL_TESTS_COL);
                cell.setCellValue(right2left_text + strSubPrivateReport);
                PrivateTravelRowCount++;
                strPrivateReport += strSubPrivateReportIndicators+strSubPrivateReport + "\n";
                privateCountRowsWithErr++;
            }
//			if(privateCountRows%1468 == 1) {
//				System.out.println(new Date() + " " + privateCountRows);
//			}

//			if(privateCountRowsWithErr>privateCountRows) {
//				System.out.println();
//			}

        }
//		System.out.println(new Date() + " " + privateCountRows);

        HashMap<String,String> reports = new HashMap<>();
        reports.put(SECTION_REPORT_INPUT_FILE, strSectionReport);
        reports.put(PRIVATE_TRAVEL_INPUT_FILE, strPrivateReport);
        return reports;
    }


    private static String addToSectionOutput(int testCol, String error, Row row, FormulaEvaluator objFormulaEvaluator) {

        DataFormatter objDefaultFormat = new DataFormatter();
        Cell cell = row.getCell(testCol);
        sectionTestCount[testCol - SECTION_R_OUTPUT_TEST01_COL]++;
        objFormulaEvaluator.evaluate(cell);
        String str = objDefaultFormat.formatCellValue(cell,objFormulaEvaluator).trim();
        cell.setCellValue(str + error);

        return error;
    }

    private static String addToPrivateOutput(int testCol, String error, Row row, FormulaEvaluator objFormulaEvaluator) {

        DataFormatter objDefaultFormat = new DataFormatter();
        Cell cell = row.getCell(testCol);
        privateTestCount[testCol - PRIVATE_R_OUTPUT_TEST01_COL]++;
        objFormulaEvaluator.evaluate(cell);
        String str = objDefaultFormat.formatCellValue(cell,objFormulaEvaluator).trim();
        cell.setCellValue(str + error);

        return error;
    }

    @SuppressWarnings("deprecation")
    private static boolean isFreeDay(String strDate) throws ParseException {
        if(nullStrings.contains((strDate +"").toLowerCase())) {
            return false;
        }
        strDate = strDate.trim();
//		String str2 = strDate.substring(0,2);
//		int day = Integer.valueOf(strDate.substring(0,2));
//		str2 = strDate.substring(3,5);
//		int month = Integer.valueOf(strDate.substring(3,5));
//		str2 = strDate.substring(6);
//		int year = Integer.valueOf(strDate.substring(6));
//		Date date = new Date(day,month,year);
        SimpleDateFormat myDate = new SimpleDateFormat("dd/MM/yyyy");
        myDate.setTimeZone(TimeZone.getTimeZone("GMT+3"));
        Date date = myDate.parse(strDate);
        int dayOfTheWeek = date.getDay();
        return (dayOfTheWeek == 5)||(dayOfTheWeek == 6);
    }

    private static long timeToSeconds(String time) throws NumberFormatException{
        long totalSeconds = 0;
        time = time.trim();
        int index1 = time.indexOf(":");
        int index2 = time.lastIndexOf(":");
        if(index2==-1){
            index2 = 0;
            index1 = 0;
        }
//		String str2 = time.substring(0,2);
        int hours = Integer.parseInt(time.substring(0,index1));
//		str2 = time.substring(3,5);
        int minutes = Integer.parseInt(time.substring(index1+1,index2));
//		str2 = time.substring(6);
        int seconds = Integer.parseInt(time.substring(index2+1));
        totalSeconds = hours*60*60 + minutes*60+ seconds;
        return totalSeconds;
    }

    private static String secondsToTime(long totalSeconds){

        long seconds = totalSeconds%60;
        totalSeconds /= 60;
        long minutes = totalSeconds%60;
        totalSeconds /= 60;
        long hours = totalSeconds;
        String time = (hours<10?"0" + hours:hours) + ":" +
                (minutes<10?"0" + minutes:minutes) + ":" +
                (seconds<10?"0" + seconds:seconds);
        return time;
    }

//	private static long dateToDays(String date) throws NumberFormatException{
//		long totalSeconds = 0;
//		date = date.trim();
//		String str2 = date.substring(0,2);
//		int hours = Integer.valueOf(date.substring(0,2));
//		str2 = date.substring(3,5);
//		int minutes = Integer.valueOf(date.substring(3,5));
//		str2 = date.substring(6);
//		int seconds = Integer.valueOf(date.substring(6));
//		totalSeconds = hours*60*60 + minutes*60+ seconds;
//		return totalSeconds;
//	}

    private static boolean comparisonDoublesABS(double a, double b, double maxError) {
        return Math.abs(a-b) <= maxError;
    }
    private static boolean comparisonDoubles(double bigNum, double smallNum, double maxError) {
        if(bigNum < smallNum) {
            return false;
        }
        return bigNum - smallNum <= maxError;
    }

    private static boolean threeStringEqualsValidation(String strStartTime, String strPreEndTime, String strPreEndTime2) {
        return ((null != strPreEndTime)&&(null != strPreEndTime2)&&(null != strStartTime)
                &&(strPreEndTime.trim().equalsIgnoreCase(strPreEndTime2.trim())&&strPreEndTime.trim().equalsIgnoreCase(strStartTime.trim())));
    }

    @SuppressWarnings("deprecation")
    private static String getStringFromCol(Cell cell, FormulaEvaluator formulaEvaluator) {
        String str = null;
        if(formulaEvaluator.evaluateInCell(cell).getCellType() == Cell.CELL_TYPE_STRING)
            str = cell.getStringCellValue();
        else if(formulaEvaluator.evaluateInCell(cell).getCellType() == Cell.CELL_TYPE_NUMERIC)
            str = cell.getNumericCellValue() + "";
        else if(formulaEvaluator.evaluateInCell(cell).getCellType() == Cell.CELL_TYPE_BOOLEAN)
            str = cell.getBooleanCellValue()+ "";

        return str + "";
    }

    @SuppressWarnings("unused")
    private static String getFieldsIndicators(String xlsFileName, String toFind, String toNotFind) throws IOException {
        FileInputStream fisSection = new FileInputStream(new File(xlsFileName));
        HSSFWorkbook workbookSection = new HSSFWorkbook(fisSection);
        HSSFSheet sheetSection = workbookSection.getSheetAt(0);
        Iterator<Row> iteratorSection = sheetSection.iterator();
        Row row = iteratorSection.next();
        String str = "";
        for(int i = 0;i<row.getLastCellNum();i++) {
            String cell = row.getCell(i).getStringCellValue().toLowerCase();
            if(cell.contains(toFind.toLowerCase())&&!cell.contains(toNotFind.toLowerCase())) {
                str += "true ,";
            }
            else {
                str += "false,";
            }
        }
        workbookSection.close();
        return str;
    }


    private static void closePrintWriter(PrintWriter pw,String fileName ){
        pw.close();
        File file = new File(fileName);
        file.delete();
    }

}
